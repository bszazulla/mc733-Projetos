/***************************************************************
* MC733 - Laboratório de Arquitetura de Computadores
* Projeto 1 - Benchmark
*
* Author: Beatriz Sechin Zazulla; Guilherme Higa Santana
* Alan Corrales
* RA: 154779; 160162; ?
*
* Description: programa para a avaliacao do benchmark AES Crypt
****************************************************************/
#include "stdio.h"

#define TC 1000
#define BRANCHES 100
#define CACHE_L1 10000000


int main(int argc, char const *argv[])
{
	double tc, pf, b, bm, l1l, l1m, result;

	printf("Por favor, passe as saidas do perf conforme solicitado:\n");

	printf("Task clock (msec):\n");
	scanf("%lf", &tc);

	printf("Page Faults:\n");
	scanf("%lf", &pf);

	printf("Branches:\n");
	scanf("%lf", &b);

	printf("Branches misses:\n");
	scanf("%lf", &bm);

	printf("L1 loads:\n");
	scanf("%lf", &l1l);

	printf("L1 load misses:\n");
	scanf("%lf", &l1m);


	result = (tc/TC + pf + (b/bm)/BRANCHES + (l1l/l1m)/CACHE_L1)/4;


	printf("--------------------------------------------------------------\n\n");
	printf("A nota do seu processador segundo AES Benchmark é %lf\n\n", result);
	printf("--------------------------------------------------------------\n\n");

	return 0;
}