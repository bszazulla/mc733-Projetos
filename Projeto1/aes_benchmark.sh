################################################################
# MC733 - Laboratório de Arquitetura de Computadores
# Projeto 1 - Benchmark
#
# Author: Beatriz Sechin Zazulla; Guilherme Higa Santana
# Alan Corrales
# RA: 154779; 160162; ?
#
# Description: Script para a execução do Benchmark AES Crypt
# Apenas execute esse script se estiver instalado as ferramen-
# tas necessarias e de posse do arquivo benchmark.mp4
#
# A chave criptográfica do AES é sempre 'mc733' nesse benchmark
#
# https://gitlab.com/bszazulla/mc733-Projetos
################################################################

#!/bin/bash

FILE="benchmark.mp4"
KEY="mc733"

# make
make

echo "Executando AES Crypt Benchmark. Aguarde..."

# execucao do benchmark
(perf stat -d ./aescrypt -e -p $KEY $FILE)

# progama que calcula 
./aes_benchmark



