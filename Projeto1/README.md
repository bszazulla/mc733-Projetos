# Programa 8 - AES Crypt
## O que faz? Para que serve?
AES Crypt é um software que tem como função a criptografia de arquivos com senha, utilizando o algoritmo simétrico AES (Advanced Encryption Standard).

## Por que é bom para medir desempenho?
Que tipo de atividade ele está medindo? Somente desempenho do processador? Desempenho do disco? Algo mais? Processador single-core, multi-core?

O software requer um tempo de processamento considerável, que aumenta proporcionalmente com o tamanho do arquivo sendo criptografado. Além disso, por estarmos lidando com um arquivos em disco, conseguimos obter métricas de I/O, memória principal e Cache.

## O que baixar

Download do programa: https://www.aescrypt.com/download/
Escolha o pacote de acordo com seu SO. Recomendamos o "AES Crypt source code (Linux)", que instalará apenas o essencial para que o benchmark funcione (as outras opções possuem a interface gráfica, que não será utilizada em nosso teste).

## Como compilar/instalar
Após fazer o download, abrir um terminal e usar cd até a pasta do arquivo.
Descompactar com: 
* tar -vzxf aescrypt-3.13.tgz

Vai aparecer um diretório "aescrypt-3.13". Use: 
* cd /aescrypt-3.13/src
* make

Os arquivos objeto estarão no diretório agora, e não é necessário realizar mais nenhum passo para poder utilizá-lo. Porém, caso você queira instalá-lo, basta
digitar: 
* make install 

ou copiá-los manualmente para onde preferir. Esta ação pode requerer permissão do administrador. Após a instalação, digite:

* make clean

para excluir os binários da pasta /src. Após a execução, digite:

* make uninstall

para desinstalar.
## Como executar

A linha de comando utilizada para criptogragar um arquivo com o AES Crypt é:

* ./aescrypt -e -p <senha> <arquivo>

Note que o ./ é necessário apenas no caso onde o usuário não realizou a instalação. Para executar o teste com o arquivo específico do benchmark, use:

* ./aescrypt -e -p mc733 benchmark.mp4

Com o terminal na pasta em que foi extraído o AES e o arquivo de benchmark disponibilizado.

## Como medir o desempenho

O desempenho da execução do AES Crypt será medido através do comando perf do Linux. Dentre os parâmetros retornados por ele, decidimos utilizar:

* task-clock (ms)
* cache L1 loads e cache L1 misses
* branches e branches misses
* page-faults

O três primeiros parâmetros medem o desempenho da CPU (o primeiro calcula o tempo de processamento que o programa levou, o segundo o desempenho da cache L1 e o terceiro a eficiência do branch predictor). O quarto mede o desempenho da memória principal (pois analisa a quantidade de vezes que uma informação não estava na memória e teve que ser recuperada do HD).
Por apresentar um tempo de execução razoavelmente longo, o teste poderá ser executado apenas uma vez.

Para obter a pontuação do benchmark, basta executar:

* ./aescrypt -e -p mc733 benchmark.mp4

Com os valores em mãos, execute:

* gcc aes_benchmark.c -o aes
* ./aes

E insira os parâmetros obtidos com o perf.

## Como apresentar o desempenho
O desempenho será mostrado como um número, com precisão de seis casas decimais, que varia de 0 a 100.

## Medições base (uma máquina)
* Intel Core i7-4510U @ 2.00GHz 
* 8 GB Memória RAM
* HD 500GB 5400RPM

Pontuação: 54.364992
