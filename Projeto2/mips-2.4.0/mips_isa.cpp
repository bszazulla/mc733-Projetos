/**
 * @file      mips_isa.cpp
 * @author    Sandro Rigo
 *            Marcus Bartholomeu
 *            Alexandro Baldassin (acasm information)
 *
 *            The ArchC Team
 *            http://www.archc.org/
 *
 *            Computer Systems Laboratory (LSC)
 *            IC-UNICAMP
 *            http://www.lsc.ic.unicamp.br/
 *
 * @version   1.0
 * @date      Mon, 19 Jun 2006 15:50:52 -0300
 * 
 * @brief     The ArchC i8051 functional model.
 * 
 * @attention Copyright (C) 2002-2006 --- The ArchC Team
 *
 */

#include  "mips_isa.H"
#include  "mips_isa_init.cpp"
#include  "mips_bhv_macros.H"


//If you want debug information for this model, uncomment next line
//#define DEBUG_MODEL
#include "ac_debug_model.H"

#include <iostream>
#include <stdio.h>
#include <string>
#include <string.h>
#include <map>
#include <vector>

//!User defined macros to reference registers.
#define Ra 31
#define Sp 29

// define pipeline stages 
char P_5[5][5] = {"IF", "ID", "EX", "MEM", "WB"};
char P_7[7][5] = {"IF", "ID", "EX1", "EX2", "MT", "MM", "WB"};
char P_13[13][5] = {"IF1", "IF2", "ID1", "ID2", "RB", "EX1", "EX2", "EX3", "EX4", "MT", "MM1", "MM2", "WB"};

// important: about pipeline stages:
// PIPELINE_5 (5 stages) -> IF - ID - EX - MEM - WB
// PIPELINE_7 (7 stages) -> IF - ID - EX1 - EX2 - MT - MM - WB (a construção desse pipeline foi baseada em exercícios encontrados na internet - DIZER NO RELATORIO) 
// MT computa o endereço de memória a ser acessado e MM faz o acesso efetivo na memoria. 
// PIPELINE_13 (13 stages) -> IF1 - IF2 - ID1 - ID2 - RB - EX1 - EX2 - EX3 - EX4 - MT - MM1 - MM2 - WB (PIPELINE INVENTADO- EXPLICAR NO RELATORIO) 
 
// 'using namespace' statement to allow access to all
// mips-specific datatypes
using namespace mips_parms;

static int processors_started = 0;
#define DEFAULT_STACK_SIZE (256*1024)

extern "C" {
		#include "../d4-7/d4.h"
}

// Branch predictor variables
// ( ͡° ͜ʖ ͡°)
int number_stalls_normal = 0; // Number of stalls in a 'not-taken branch' optimized prediction
int number_stalls_pb = 0; // Number of stalls using a dynamic prediction buffer (saturating counter)
int number_stalls_tlap = 0; // Number of stalls in a Two-level Adaptive predictor
std::map<int, int> pb; // Prediction buffer
std::map<int, std::vector<int>> tlap_pb; // Two-level Adaptive predictor prediction buffer
std::map<int, int> tlap_oc; // Two-level Adaptive predictor ocurrences

// Branch predictor methods signature
void branch_taken(int ac);
void branch_not_taken(int ac);

//! Instruction Format behavior methods.
void ac_behavior( Type_R ){}
void ac_behavior( Type_I ){
	
	// Insert branch in dictionary
	pb.insert(std::pair<int,int>((int)ac_pc, 0));
	std::vector<int> new_vec (4,0);
	tlap_pb.insert(std::pair<int, std::vector<int>>((int)ac_pc, new_vec));
	tlap_oc.insert(std::pair<int, int>((int)ac_pc, 0));

}
void ac_behavior( Type_J ){}

// hazard struct for the pipelines (5, 7, 11 stages)
typedef struct Pipeline
{
	char instruction[10]; // instruction opcode
	char stage[5]; // stage in pipeline
	int rs, rt, rd; // R-format instructions have the three. I-format have only rs, rt
	bool is_load;

} Pipeline;

// hazards and cycles variables 
// ┬┴┬┴┤ ͜ʖ ͡°) ├┬┴┬┴
int total_cycles_forwarding, total_cycles_noforwarding; // will be executed instructions + cycles from hazards (depends on forwarding)
int total_instructions_executed = 0;

// control hazards in cycles
int number_stalls_control_hazards_5 = 0, 
	number_stalls_control_hazards_7 = 0;

bool forwarding; // testing forwarding or not
// data hazards in cycles


// ATENÇÃO AQUI -> IMPORTANTE


int number_stalls_data_hazards_5_forwarding = 0, // inevitaveis mesmo com forwarding, 5-stage pipeline
	number_stalls_data_hazards_5_noforwarding = 0, // sem forwarding, 5-stage pipeline
	number_stalls_data_hazards_7_forwarding = 0, // inevitaveis mesmo com forwarding, 7-stage pipeline
	number_stalls_data_hazards_7_noforwarding = 0; // sem forwarding, 7-stage pipeline


std::vector<Pipeline> pipeline_5, pipeline_7; // pipelines (filo struct)

// about hazards
/* EX/MEM.RegisterRd = ID/EX.RegisterRs 

EX/MEM.RegisterRd = ID/EX.RegisterRt 

MEM/WB.RegisterRd = ID/EX.RegisterRs 

MEM/WB.RegisterRd = ID/EX.RegisterRt */

// stall function
void stall(std::vector<Pipeline> & pipeline, int pos)
{
	Pipeline aux;

	aux.rs = -1;
	aux.rt = -1;
	aux.rd = -1;
	aux.is_load = false;

	// arrumar o pipeline com instrução "nop"
	pipeline.insert(pipeline.begin() + pos, aux);
	pipeline.pop_back();
}

// pipeline data hazards
void hazards_5()
{
	// forwarding
	if (!forwarding)
	{
		// analise do estagio EX para instruções - o EX é o estagio 2
		if (pipeline_5[2].rd != -1 && (pipeline_5[2].rd == pipeline_5[1].rs || pipeline_5[2].rd == pipeline_5[1].rt))
		{
			// computar um ciclo
			number_stalls_data_hazards_5_noforwarding++;

			stall(pipeline_5, 2);
		}

		// analisa registrador de memoria MEM
		if (pipeline_5[3].rd != -1 && (pipeline_5[3].rd == pipeline_5[1].rs || pipeline_5[3].rd == pipeline_5[1].rt))
		{
			// computar um ciclo
			number_stalls_data_hazards_5_noforwarding++;

			stall(pipeline_5, 3);
		}
	}

	// hazards de load-use, que não importam o forwarding
	if (pipeline_5[1].is_load && (pipeline_5[1].rt == pipeline_5[0].rt || pipeline_5[1].rt == pipeline_5[0].rs))
	{
		number_stalls_control_hazards_5++;
		stall(pipeline_5, 1); // stall onde esta o load
	}
}

// hazards no pipeline de 7 estágios
void hazards_7()
{

	// para o EX1, temos que pensar que o dado ainda nao está pronto, portanto temos também que considerar stalls aqui
	// e aqui não tem forwarding pois realmente o resultado nao esta pronto - não damos forwarding pra tras
	// analise do estagio EX1 para instruções - o EX1 é o estagio 2
	if (pipeline_7[2].rd != -1 && (pipeline_7[2].rd == pipeline_7[1].rs || pipeline_7[2].rd == pipeline_7[1].rt))
	{
			// computar um ciclo
		number_stalls_data_hazards_7_forwarding++;

		stall(pipeline_7, 2);
	}

	// forwarding
	if (!forwarding)
	{
		// analise do estagio EX2 para instruções - o EX2 é o estagio 3
		// não dá para dar forward do EX1, tem que esperar ficar pronto no EX2
		if (pipeline_7[3].rd != -1 && (pipeline_7[3].rd == pipeline_7[1].rs || pipeline_7[3].rd == pipeline_7[1].rt))
		{
			// computar um ciclo
			number_stalls_data_hazards_7_noforwarding++;

			stall(pipeline_7, 3);
		}

		// analisa registrador de memoria MT - o MT é o estagio 4 
		if (pipeline_7[4].rd != -1 && (pipeline_7[4].rd == pipeline_7[1].rs || pipeline_7[4].rd == pipeline_7[1].rt))
		{
			// computar um ciclo
			number_stalls_data_hazards_7_noforwarding++;

			stall(pipeline_7, 4);
		}

		// analisa registrador de memoria MM - o MM é o estagio 5 
		if (pipeline_7[5].rd != -1 && (pipeline_7[5].rd == pipeline_7[1].rs || pipeline_7[5].rd == pipeline_7[1].rt))
		{
				// computar um ciclo
			number_stalls_data_hazards_7_noforwarding++;

			stall(pipeline_7, 5);
		}
	}

	// hazards de load-use, que não importam o forwarding

	// checa se a intrução anteior depende do load
	// TESTA O IF EM RELAÇÃO AO ID
	if (pipeline_7[1].is_load && (pipeline_7[1].rt == pipeline_7[0].rt || pipeline_7[1].rt == pipeline_7[0].rs))
	{
		// tres stalls, justamente por olhar os tres registradores pra tras
		number_stalls_control_hazards_7+=3;
		stall(pipeline_7, 1); // stall onde esta o load
		stall(pipeline_7, 1); // stall onde esta o load
		stall(pipeline_7, 1); // stall onde esta o load
	}

	// checa se a instrução anterior da anterior depende do load
	//TESTA EX1 E IF
	if (pipeline_7[2].is_load && (pipeline_7[2].rt == pipeline_7[0].rt || pipeline_7[2].rt == pipeline_7[0].rs))
	{
		// DOIS stalls, justamente por olhar os DOIS registradores pra tras
		number_stalls_control_hazards_7+=2;
		stall(pipeline_7, 2); // stall onde esta o load
		stall(pipeline_7, 2); // stall onde esta o load
	}

	// mais do mesmo, para os resto
	// TESTE EX2 E IF
	if (pipeline_7[3].is_load && (pipeline_7[3].rt == pipeline_7[0].rt || pipeline_7[3].rt == pipeline_7[0].rs))
	{
		// UM stall, justamente por olhar O registrador pra tras
		number_stalls_control_hazards_7+=1;
		stall(pipeline_7, 3); // stall onde esta o load
	}
}

void initilize_pipelines(std::vector<Pipeline> & pipeline_5, std::vector<Pipeline> & pipeline_7) 
{   
	int i; // counter

	// pipeline 5
	pipeline_5.resize(5);
	for (i = 0; i < 5; i++)
	{
		strcpy(pipeline_5[i].stage, P_5[i]);
		pipeline_5[i].rd = -1;
		pipeline_5[i].rt = -1;
		pipeline_5[i].rs = -1;          
	}

	// pipeline 7
	pipeline_7.resize(7);
	for (i = 0; i < 7; i++)
	{
		strcpy(pipeline_7[i].stage,P_7[i]);
		pipeline_7[i].rd = -1;
		pipeline_7[i].rt = -1;
		pipeline_7[i].rs = -1;  
	}
}

// update pipeline
void update_pipeline(char instruction[], int rd, int rt, int rs, bool is_load)
{
	Pipeline aux;

	// estrutura auxiliar do pipeline
	strcpy(aux.instruction, instruction);
	aux.rd = rd;
	aux.rs = rs;
	aux.rt = rt;
	aux.is_load = is_load;

	// new instruction on the pipeline!!
	pipeline_5.insert(pipeline_5.begin(), aux);
	pipeline_5.pop_back();
	pipeline_7.insert(pipeline_7.begin(), aux);
	pipeline_7.pop_back();

	// atualiza estagios
	for (int i = 0; i < 5; i++)
			strcpy(pipeline_5[i].stage, P_5[i]);

	for (int i = 0; i < 7; i++)
			strcpy(pipeline_7[i].stage, P_7[i]);

	hazards_5();
	hazards_7();
}


void print_pipeline(std::vector<Pipeline> & pipeline, int tam)
{
	printf("\n\n\nPIPELINE:\n");
	for (int i = 0; i < tam; ++i)
	{
			printf("[%s: %s rd: %d rs: %d rt: %d] --> ", pipeline[i].stage, pipeline[i].instruction, pipeline[i].rd, pipeline[i].rs, pipeline[i].rt);
	}
	printf("\n\n\n");
}

//cache stuff
int total_cache_accesses;

typedef struct {
	d4cache *Mem;
	d4cache *L1_d;
	d4cache *L1_i;
	d4cache *L2;
	d4cache *L3;
}CacheConfig;

//array of cache configurations
CacheConfig cache_configs[4];

//generate cache configuration 1
void generateCaches1(){
	d4cache* Mem;
	d4cache *L1_d, *L1_i, *L2, *L3;
	
	Mem = d4new(0);
	cache_configs[0].Mem = Mem;
	
	L2 = d4new(Mem);
	L2->name = (char *)"L2";
	L2->lg2blocksize = 9; //2^9 = 64B
	L2->lg2subblocksize = 0;
	L2->lg2size = 20; //2^20 = 128KB
	L2->assoc = 4;
	L2->replacementf = d4rep_lru;
	L2->prefetchf = d4prefetch_none;
	L2->wallocf = d4walloc_always;
	L2->wbackf = d4wback_always;
	L2->name_replacement = L2->name_prefetch = L2->name_walloc = L2->name_wback = (char *)"L2";
	cache_configs[0].L2= L2;
	
	L1_i = d4new(L2);
	L1_i->name = (char *)"L1_instr";
	L1_i->lg2blocksize = 8; //2^8 = 32B
	L1_i->lg2subblocksize = 0;
	L1_i->lg2size = 19; //2^19 = 64KB
	L1_i->assoc = 4;
	L1_i->replacementf = d4rep_lru;
	L1_i->prefetchf = d4prefetch_none;
	L1_i->wallocf = d4walloc_always;
	L1_i->wbackf = d4wback_always;
	L1_i->name_replacement = L1_i->name_prefetch = L1_i->name_walloc = L1_i->name_wback = (char *)"L1_instr";
	cache_configs[0].L1_i= L1_i;
	
	L1_d = d4new(L2);
	L1_d->name = (char *)"L1_data";
	L1_d->lg2blocksize = 8; //2^8 = 32B
	L1_d->lg2subblocksize = 0;
	L1_d->lg2size = 19; //2^19 = 64KB
	L1_d->assoc = 4;
	L1_d->replacementf = d4rep_lru;
	L1_d->prefetchf = d4prefetch_none;
	L1_d->wallocf = d4walloc_always;
	L1_d->wbackf = d4wback_always;
	L1_d->name_replacement = L1_d->name_prefetch = L1_d->name_walloc = L1_d->name_wback = (char *)"L1_d";
	cache_configs[0].L1_d = L1_d;
}

//generate cache configuration 2
void generateCaches2(){
	d4cache* Mem;
	d4cache *L1_d, *L1_i, *L2, *L3;
	
	Mem = d4new(0);
	cache_configs[1].Mem = Mem;

	L3 = d4new(Mem);
	L3->name = (char *)"L3";
	L3->lg2blocksize = 9; //2^9 = 64B
	L3->lg2subblocksize = 0;
	L3->lg2size = 26; //2^26 = 8MB
	L3->assoc = 4;
	L3->replacementf = d4rep_lru;
	L3->prefetchf = d4prefetch_none;
	L3->wallocf = d4walloc_always;
	L3->wbackf = d4wback_always;
	L3->name_replacement = L3->name_prefetch = L3->name_walloc = L3->name_wback = (char *)"L3";
	cache_configs[1].L3= L3;
	
	L2 = d4new(L3);
	L2->name = (char *)"L2";
	L2->lg2blocksize = 9; //2^9 = 64B
	L2->lg2subblocksize = 0;
	L2->lg2size = 20; //2^20 = 128KB
	L2->assoc = 4;
	L2->replacementf = d4rep_lru;
	L2->prefetchf = d4prefetch_none;
	L2->wallocf = d4walloc_always;
	L2->wbackf = d4wback_always;
	L2->name_replacement = L2->name_prefetch = L2->name_walloc = L2->name_wback = (char *)"L2";
	cache_configs[1].L2= L2;
	
	L1_i = d4new(L2);
	L1_i->name = (char *)"L1_instr";
	L1_i->lg2blocksize = 8; //2^8 = 32B
	L1_i->lg2subblocksize = 0;
	L1_i->lg2size = 19; //2^19 = 64KB
	L1_i->assoc = 4;
	L1_i->replacementf = d4rep_lru;
	L1_i->prefetchf = d4prefetch_none;
	L1_i->wallocf = d4walloc_always;
	L1_i->wbackf = d4wback_always;
	L1_i->name_replacement = L1_i->name_prefetch = L1_i->name_walloc = L1_i->name_wback = (char *)"L1_instr";
	cache_configs[1].L1_i= L1_i;
	
	L1_d = d4new(L2);
	L1_d->name = (char *)"L1_data";
	L1_d->lg2blocksize = 8; //2^8 = 32B
	L1_d->lg2subblocksize = 0;
	L1_d->lg2size = 19; //2^19 = 64KB
	L1_d->assoc = 4;
	L1_d->replacementf = d4rep_lru;
	L1_d->prefetchf = d4prefetch_none;
	L1_d->wallocf = d4walloc_always;
	L1_d->wbackf = d4wback_always;
	L1_d->name_replacement = L1_d->name_prefetch = L1_d->name_walloc = L1_d->name_wback = (char *)"L1_d";
	cache_configs[1].L1_d = L1_d;
}

//generate cache configuration 3
void generateCaches3(){
	d4cache* Mem;
	d4cache *L1_d, *L1_i, *L2, *L3;
	
	Mem = d4new(0);
	cache_configs[2].Mem = Mem;
	
	L2 = d4new(Mem);
	L2->name = (char *)"L2";
	L2->lg2blocksize = 9; //2^9 = 64B
	L2->lg2subblocksize = 0;
	L2->lg2size = 21; //2^21 = 256KB
	L2->assoc = 8;
	L2->replacementf = d4rep_lru;
	L2->prefetchf = d4prefetch_none;
	L2->wallocf = d4walloc_always;
	L2->wbackf = d4wback_always;
	L2->name_replacement = L2->name_prefetch = L2->name_walloc = L2->name_wback = (char *)"L2";
	cache_configs[2].L2= L2;
	
	L1_i = d4new(L2);
	L1_i->name = (char *)"L1_instr";
	L1_i->lg2blocksize = 9; //2^9 = 64B
	L1_i->lg2subblocksize = 0;
	L1_i->lg2size = 18; //2^18 = 32KB
	L1_i->assoc = 8;
	L1_i->replacementf = d4rep_lru;
	L1_i->prefetchf = d4prefetch_none;
	L1_i->wallocf = d4walloc_always;
	L1_i->wbackf = d4wback_always;
	L1_i->name_replacement = L1_i->name_prefetch = L1_i->name_walloc = L1_i->name_wback = (char *)"L1_instr";
	cache_configs[2].L1_i= L1_i;
	
	L1_d = d4new(L2);
	L1_d->name = (char *)"L1_data";
	L1_d->lg2blocksize = 9; //2^8 = 64B
	L1_d->lg2subblocksize = 0;
	L1_d->lg2size = 18; //2^18 = 32KB
	L1_d->assoc = 8;
	L1_d->replacementf = d4rep_lru;
	L1_d->prefetchf = d4prefetch_none;
	L1_d->wallocf = d4walloc_always;
	L1_d->wbackf = d4wback_always;
	L1_d->name_replacement = L1_d->name_prefetch = L1_d->name_walloc = L1_d->name_wback = (char *)"L1_d";
	cache_configs[2].L1_d = L1_d;
}

//generate cache configuration 4
void generateCaches4(){
	d4cache* Mem;
	d4cache *L1_d, *L1_i, *L2, *L3;
	
	Mem = d4new(0);
	cache_configs[3].Mem = Mem;

	L3 = d4new(Mem);
	L3->name = (char *)"L3";
	L3->lg2blocksize = 9; //2^9 = 64B
	L3->lg2subblocksize = 0;
	L3->lg2size = 26; //2^26 = 8MB
	L3->assoc = 8;
	L3->replacementf = d4rep_lru;
	L3->prefetchf = d4prefetch_none;
	L3->wallocf = d4walloc_always;
	L3->wbackf = d4wback_always;
	L3->name_replacement = L3->name_prefetch = L3->name_walloc = L3->name_wback = (char *)"L3";
	cache_configs[3].L3= L3;
	
	L2 = d4new(L3);
	L2->name = (char *)"L2";
	L2->lg2blocksize = 9; //2^9 = 64B
	L2->lg2subblocksize = 0;
	L2->lg2size = 21; //2^21 = 256KB
	L2->assoc = 8;
	L2->replacementf = d4rep_lru;
	L2->prefetchf = d4prefetch_none;
	L2->wallocf = d4walloc_always;
	L2->wbackf = d4wback_always;
	L2->name_replacement = L2->name_prefetch = L2->name_walloc = L2->name_wback = (char *)"L2";
	cache_configs[3].L2= L2;
	
	L1_i = d4new(L2);
	L1_i->name = (char *)"L1_instr";
	L1_i->lg2blocksize = 9; //2^9 = 64B
	L1_i->lg2subblocksize = 0;
	L1_i->lg2size = 18; //2^18 = 32KB
	L1_i->assoc = 8;
	L1_i->replacementf = d4rep_lru;
	L1_i->prefetchf = d4prefetch_none;
	L1_i->wallocf = d4walloc_always;
	L1_i->wbackf = d4wback_always;
	L1_i->name_replacement = L1_i->name_prefetch = L1_i->name_walloc = L1_i->name_wback = (char *)"L1_instr";
	cache_configs[3].L1_i= L1_i;
	
	L1_d = d4new(L2);
	L1_d->name = (char *)"L1_data";
	L1_d->lg2blocksize = 9; //2^8 = 64B
	L1_d->lg2subblocksize = 0;
	L1_d->lg2size = 18; //2^18 = 32KB
	L1_d->assoc = 8;
	L1_d->replacementf = d4rep_lru;
	L1_d->prefetchf = d4prefetch_none;
	L1_d->wallocf = d4walloc_always;
	L1_d->wbackf = d4wback_always;
	L1_d->name_replacement = L1_d->name_prefetch = L1_d->name_walloc = L1_d->name_wback = (char *)"L1_d";
	cache_configs[3].L1_d = L1_d;
}

//simulate read in the cache configuration
void doread_instrn(d4addr addr) {
	d4memref R;
	R.address = addr;
	R.size = 4;
	R.accesstype = D4XINSTRN;
	
total_cache_accesses++;
	for(int i = 0 ; i < 4; i++)
			d4ref(cache_configs[i].L1_i, R);
}

//simulate read in the cache configuration
void doread(d4addr addr) {
	d4memref R;
	R.address = addr;
	R.size = 4;
	R.accesstype = D4XREAD;
	
total_cache_accesses++;
	for(int i = 0 ; i < 4; i++)
			d4ref(cache_configs[i].L1_d, R);
}

//simulate write in the cache configuration
void dowrite(d4addr addr) {
	d4memref R;
	R.address = addr;
	R.size = 4;
	R.accesstype = D4XWRITE;
	
total_cache_accesses++;
	for(int i = 0 ; i < 4; i++)
			d4ref(cache_configs[i].L1_d, R);
}

//!Generic instruction behavior method.
void ac_behavior( instruction )
{
	doread_instrn(ac_pc);
	dbg_printf("----- PC=%#x ----- %lld\n", (int) ac_pc, ac_instr_counter);
	//  dbg_printf("----- PC=%#x NPC=%#x ----- %lld\n", (int) ac_pc, (int)npc, ac_instr_counter);
#ifndef NO_NEED_PC_UPDATE
	ac_pc = npc;
	npc = ac_pc + 4;
#endif 
};

//!Behavior called before starting simulation
void ac_behavior(begin)
{
	dbg_printf("@@@ begin behavior @@@\n");
	RB[0] = 0;
	npc = ac_pc + 4;
	int i;

	// Is is not required by the architecture, but makes debug really easier
	for (int regNum = 0; regNum < 32; regNum ++)
		RB[regNum] = 0;
	hi = 0;
	lo = 0;

	RB[29] =  AC_RAM_END - 1024 - processors_started++ * DEFAULT_STACK_SIZE;
	
	total_cache_accesses = 0;
	generateCaches1();
	generateCaches2();
	generateCaches3();
	generateCaches4();

	// initialize pipelines
 	forwarding = false;
 	initilize_pipelines(pipeline_5, pipeline_7); 

	int r;
	if (0 != (r = d4setup()))
		std::cout << "Failed\n";
}

//!Behavior called after finishing simulation
void ac_behavior(end)
{
	dbg_printf("@@@ end behavior @@@\n");
	
	// Cache simulation results.

	printf("------------------------------------------------------------\n\n");
	printf("---------------------- PARCIAL RESULTS ---------------------\n\n");
	printf("CACHE RESULTS:\n");
	printf("Number of memory accesses: \t%d\n", total_cache_accesses);
	for (int i = 0; i < 4; i++) {
		printf("------------------------------------------------------------\n");
		printf("Cache configuration %d:\n", i);
		int L2_hit_instrn = cache_configs[i].L2 -> fetch[D4XINSTRN] * 10;
		int L2_hit_read = cache_configs[i].L2 -> fetch[D4XREAD] * 10;
		int L2_hit_write = cache_configs[i].L2 -> fetch[D4XWRITE] * 10;

		int L1d_hit_instrn = cache_configs[i].L1_d -> fetch[D4XINSTRN] * 4;
		int L1d_hit_read = cache_configs[i].L1_d -> fetch[D4XREAD] * 4;
		int L1d_hit_write = cache_configs[i].L1_d -> fetch[D4XWRITE] * 4;

		int L1i_hit_instrn = cache_configs[i].L1_i -> fetch[D4XINSTRN] * 4;
		int L1i_hit_read = cache_configs[i].L1_i -> fetch[D4XREAD] * 4;
		int L1i_hit_write = cache_configs[i].L1_i -> fetch[D4XWRITE] * 4;

		int MEM_hit_instrn = cache_configs[i].Mem -> fetch[D4XINSTRN] * 120;
		int MEM_hit_read = cache_configs[i].Mem -> fetch[D4XREAD] * 120;
		int MEM_hit_write = cache_configs[i].Mem -> fetch[D4XWRITE] * 120;

		int L3_hit_instrn = 0;
		int L3_hit_read = 0;
		int L3_hit_write = 0;
		if(cache_configs[i].L3 != NULL){
		  L3_hit_instrn = cache_configs[i].L3 -> fetch[D4XINSTRN] * 65;
		  L3_hit_read = cache_configs[i].L3 -> fetch[D4XREAD] * 65;
		  L3_hit_write = cache_configs[i].L3 -> fetch[D4XWRITE] * 65;
		}

		printf("L1-Data\n");
		printf("Instruction read hit cycles: %d\n", L1d_hit_instrn);
		printf("Data read hit cycles: %d\n", L1d_hit_read);
		printf("Data write hit cycles: %d\n", L1d_hit_write);
		printf("Total hit cycles: %d\n\n", L1d_hit_instrn + L1d_hit_read + L1d_hit_write);

		printf("L1-Instructions\n");
		printf("Instruction read hit cycles: %d\n", L1i_hit_instrn);
		printf("Data read hit cycles: %d\n", L1i_hit_read);
		printf("Data write hit cycles: %d\n", L1i_hit_write);
		printf("Total hit cycles: %d\n\n", L1i_hit_instrn + L1i_hit_read + L1i_hit_write);

		printf("L2\n");
		printf("Instruction read hit cycles: %d\n", L2_hit_instrn);
		printf("Data read hit cycles: %d\n", L2_hit_read);
		printf("Data write hit cycles: %d\n", L2_hit_write);
		printf("Total hit cycles: %d\n\n", L2_hit_instrn + L2_hit_read + L2_hit_write);

		if(cache_configs[i].L3 != NULL){
		  printf("L3\n");
		  printf("Instruction read hit cycles: %d\n", L3_hit_instrn);
		  printf("Data read hit cycles: %d\n", L3_hit_read);
		  printf("Data write hit cycles: %d\n", L3_hit_write);
		  printf("Total hit cycles: %d\n\n", L3_hit_instrn + L3_hit_read + L3_hit_write);
		}

		printf("Memory\n");
		printf("Instruction read hit cycles: %d\n", MEM_hit_instrn);
		printf("Data read hit cycles: %d\n", MEM_hit_read);
		printf("Data write hit cycles: %d\n", MEM_hit_write);
		printf("Total hit cycles: %d\n\n", MEM_hit_instrn + MEM_hit_read + MEM_hit_write);
		
		//printf("TOTAL CYCLES SPENT ACCESSING MEMORY: %d\n\n", L1d_hit_instrn + L1d_hit_read + L1d_hit_write + L1i_hit_instrn + L1i_hit_read + L1i_hit_write +
			//L2_hit_instrn + L2_hit_read + L2_hit_write + L3_hit_instrn + L3_hit_read + L3_hit_write);
	}
	
	// number of hazards
	printf("\n------------------------------------------------------------\n\n");
	printf("HAZARDS RESULTS:\n\n");
	printf("With forwarding:\n");
	printf("Control Hazards in 5-stage pipeline: \t\t%d\n", number_stalls_control_hazards_5);
	printf("Control Hazards in 7-stage pipeline: \t\t%d\n", number_stalls_control_hazards_7);
	printf("Data Hazards in 5-stage pipeline: \t\t%d\n", number_stalls_data_hazards_5_forwarding);
	printf("Data Hazards in 7-stage pipeline: \t\t%d\n", number_stalls_data_hazards_7_forwarding);
	printf("\n\nWithout forwarding:\n");
	printf("Control Hazards in 5-stage pipeline: \t\t%d\n", number_stalls_control_hazards_5);
	printf("Control Hazards in 7-stage pipeline: \t\t%d\n", number_stalls_control_hazards_7);
	// hazards inevitaveis MESMO COM FORWARDING + os sem forwarding
	printf("Data Hazards in 5-stage pipeline: \t\t%d\n", number_stalls_data_hazards_5_noforwarding + number_stalls_data_hazards_5_forwarding);
	printf("Data Hazards in 7-stage pipeline: \t\t%d\n\n", number_stalls_data_hazards_7_noforwarding + number_stalls_data_hazards_5_forwarding);
	printf("\n------------------------------------------------------------\n\n");

	// Branch prediction results.
	printf("BRANCH PREDICTION RESULTS:\n\n");
	printf("Type of prediction: \t\tTimes wrong\n");
	printf("Static prediction (always not): \t%d\n", number_stalls_normal);
	printf("Dynamic prediction buffer: \t\t%d\n", number_stalls_pb);
	printf("Two-level Adaptive buffer: \t\t%d\n", number_stalls_tlap);

	printf("------------------------------------------------------------\n\n");
	printf("------------------ FINAL CYCLES RESULTS ---------------------\n");
	printf("Total instructions executed: \t%d\n", total_instructions_executed);
	printf("Total cycles with forwarding in 5-stage pipeline: \t%d\n", total_instructions_executed + number_stalls_control_hazards_5 + number_stalls_data_hazards_5_forwarding);
	printf("Total cycles with forwarding in 7-stage pipeline: \t%d\n",total_instructions_executed + number_stalls_control_hazards_7 + number_stalls_data_hazards_7_forwarding);
	printf("Total cycles without forwarding in 5-stage pipeline: \t%d\n", total_instructions_executed + number_stalls_control_hazards_5 + number_stalls_data_hazards_5_forwarding + number_stalls_data_hazards_5_noforwarding);
	printf("Total cycles without forwarding in 7-stage pipeline: \t%d\n",total_instructions_executed + number_stalls_control_hazards_7 + number_stalls_data_hazards_7_forwarding + number_stalls_data_hazards_7_noforwarding);
	printf("------------------------------------------------------------\n\n");
}


//!Instruction lb behavior method.
void ac_behavior( lb )
{
	char byte;
	dbg_printf("lb r%d, %d(r%d)\n", rt, imm & 0xFFFF, rs);
	byte = DATA_PORT->read_byte(RB[rs]+ imm);
	RB[rt] = (ac_Sword)byte ;
	dbg_printf("Result = %#x\n", RB[rt]);
	
	long aux = (long)(RB[rs]+ imm);
	doread(aux);

	// update pipeline 5
	update_pipeline((char*)"lb", -1, rt, rs, true);

	total_instructions_executed++;
};

//!Instruction lbu behavior method.
void ac_behavior( lbu )
{
	unsigned char byte;
	dbg_printf("lbu r%d, %d(r%d)\n", rt, imm & 0xFFFF, rs);
	byte = DATA_PORT->read_byte(RB[rs]+ imm);
	RB[rt] = byte ;
	dbg_printf("Result = %#x\n", RB[rt]);
	
	long aux = (long)(RB[rs]+ imm);
	doread(aux);

	// update pipeline 5
	update_pipeline((char*)"lbu", -1, rt, rs, true);

	total_instructions_executed++; 
};

//!Instruction lh behavior method.
void ac_behavior( lh )
{
	short int half;
	dbg_printf("lh r%d, %d(r%d)\n", rt, imm & 0xFFFF, rs);
	half = DATA_PORT->read_half(RB[rs]+ imm);
	RB[rt] = (ac_Sword)half ;
	dbg_printf("Result = %#x\n", RB[rt]);
	
	long aux = (long)(RB[rs]+ imm);
	doread(aux);

	// update pipeline 5
	update_pipeline((char*)"lh", -1, rt, rs, true);
 
	total_instructions_executed++;
};

//!Instruction lhu behavior method.
void ac_behavior( lhu )
{
	unsigned short int  half;
	half = DATA_PORT->read_half(RB[rs]+ imm);
	RB[rt] = half ;
	dbg_printf("Result = %#x\n", RB[rt]);
	
	long aux = (long)(RB[rs]+ imm);
	doread(aux);

	// update pipeline 5
	update_pipeline((char*)"lhu", -1, rt, rs, true);
 
	total_instructions_executed++;
};

//!Instruction lw behavior method.
void ac_behavior( lw )
{
	dbg_printf("lw r%d, %d(r%d)\n", rt, imm & 0xFFFF, rs);
	RB[rt] = DATA_PORT->read(RB[rs]+ imm);
	dbg_printf("Result = %#x\n", RB[rt]);
	
	long aux = (long)(RB[rs]+ imm);
	doread(aux);

	// update pipeline 5
	update_pipeline((char*)"lw", -1, rt, rs, true);
 
	total_instructions_executed++;
};

//!Instruction lwl behavior method.
void ac_behavior( lwl )
{
	dbg_printf("lwl r%d, %d(r%d)\n", rt, imm & 0xFFFF, rs);
	unsigned int addr, offset;
	ac_Uword data;

	addr = RB[rs] + imm;
	offset = (addr & 0x3) * 8;
	data = DATA_PORT->read(addr & 0xFFFFFFFC);
	data <<= offset;
	data |= RB[rt] & ((1<<offset)-1);
	RB[rt] = data;
	dbg_printf("Result = %#x\n", RB[rt]);
	
	long aux = (long)(addr & 0xFFFFFFFC);
	doread(aux);

	// update pipeline 5
	update_pipeline((char*)"lwl", -1, rt, rs, true);

	total_instructions_executed++;
};

//!Instruction lwr behavior method.
void ac_behavior( lwr )
{
	dbg_printf("lwr r%d, %d(r%d)\n", rt, imm & 0xFFFF, rs);
	unsigned int addr, offset;
	ac_Uword data;

	addr = RB[rs] + imm;
	offset = (3 - (addr & 0x3)) * 8;
	data = DATA_PORT->read(addr & 0xFFFFFFFC);
	data >>= offset;
	data |= RB[rt] & (0xFFFFFFFF << (32-offset));
	RB[rt] = data;
	dbg_printf("Result = %#x\n", RB[rt]);
	
	long aux = (long)(addr & 0xFFFFFFFC);
	doread(aux);

	 // update pipeline 5
	update_pipeline((char*)"lwr", -1, rt, rs, true);
 
	total_instructions_executed++;
};

//!Instruction sb behavior method.
void ac_behavior( sb )
{
	unsigned char byte;
	dbg_printf("sb r%d, %d(r%d)\n", rt, imm & 0xFFFF, rs);
	byte = RB[rt] & 0xFF;
	DATA_PORT->write_byte(RB[rs] + imm, byte);
	dbg_printf("Result = %#x\n", (int) byte);
	
	long aux = (long)(RB[rs]+ imm);
	dowrite(aux);

	// update pipeline 5
	update_pipeline((char*)"sb", -1, rt, rs, false);
 
	total_instructions_executed++;
};

//!Instruction sh behavior method.
void ac_behavior( sh )
{
	unsigned short int half;
	dbg_printf("sh r%d, %d(r%d)\n", rt, imm & 0xFFFF, rs);
	half = RB[rt] & 0xFFFF;
	DATA_PORT->write_half(RB[rs] + imm, half);
	dbg_printf("Result = %#x\n", (int) half);
	
	long aux = (long)(RB[rs]+ imm);
	dowrite(aux);

	// update pipeline 5
	update_pipeline((char*)"sh", -1, rt, rs, false);
 
	total_instructions_executed++;
};

//!Instruction sw behavior method.
void ac_behavior( sw )
{
	dbg_printf("sw r%d, %d(r%d)\n", rt, imm & 0xFFFF, rs);
	DATA_PORT->write(RB[rs] + imm, RB[rt]);
	dbg_printf("Result = %#x\n", RB[rt]);
	
	long aux = (long)(RB[rs]+ imm);
	dowrite(aux);

	// update pipeline 5
	update_pipeline((char*)"sw", -1, rt, rs, false);
 
	total_instructions_executed++;
};

//!Instruction swl behavior method.
void ac_behavior( swl )
{
	dbg_printf("swl r%d, %d(r%d)\n", rt, imm & 0xFFFF, rs);
	unsigned int addr, offset;
	ac_Uword data;

	addr = RB[rs] + imm;
	offset = (addr & 0x3) * 8;
	data = RB[rt];
	data >>= offset;
	data |= DATA_PORT->read(addr & 0xFFFFFFFC) & (0xFFFFFFFF << (32-offset));
	DATA_PORT->write(addr & 0xFFFFFFFC, data);
	dbg_printf("Result = %#x\n", data);
	
	long aux1 = (long)(addr & 0xFFFFFFFC);
	long aux2 = (RB[rs]+ imm);
	doread(aux1);
	dowrite(aux2);

	// update pipeline 5
	update_pipeline((char*)"swl", -1, rt, rs, false);
 
	total_instructions_executed++;
};

//!Instruction swr behavior method.
void ac_behavior( swr )
{
	dbg_printf("swr r%d, %d(r%d)\n", rt, imm & 0xFFFF, rs);
	unsigned int addr, offset;
	ac_Uword data;

	addr = RB[rs] + imm;
	offset = (3 - (addr & 0x3)) * 8;
	data = RB[rt];
	data <<= offset;
	data |= DATA_PORT->read(addr & 0xFFFFFFFC) & ((1<<offset)-1);
	DATA_PORT->write(addr & 0xFFFFFFFC, data);
	dbg_printf("Result = %#x\n", data);
	
	long aux1 = (long)(addr & 0xFFFFFFFC);
	long aux2 = (RB[rs]+ imm);
	doread(aux1);
	dowrite(aux2);

	// update pipeline 5
	update_pipeline((char*)"swr", -1, rt, rs, false);
 
	total_instructions_executed++;
};

//!Instruction addi behavior method.
void ac_behavior( addi )
{
	dbg_printf("addi r%d, r%d, %d\n", rt, rs, imm & 0xFFFF);
	RB[rt] = RB[rs] + imm;
	dbg_printf("Result = %#x\n", RB[rt]);
	//Test overflow
	if ( ((RB[rs] & 0x80000000) == (imm & 0x80000000)) &&
			 ((imm & 0x80000000) != (RB[rt] & 0x80000000)) ) {
		fprintf(stderr, "EXCEPTION(addi): integer overflow.\n"); exit(EXIT_FAILURE);
	}

	// update pipeline 5
	update_pipeline((char*)"addi", -1, rt, rs, false);
 
	total_instructions_executed++;
};

//!Instruction addiu behavior method.
void ac_behavior( addiu )
{
	dbg_printf("addiu r%d, r%d, %d\n", rt, rs, imm & 0xFFFF);
	RB[rt] = RB[rs] + imm;
	dbg_printf("Result = %#x\n", RB[rt]);

	// update pipeline 5
	update_pipeline((char*)"addiu", -1, rt, rs, false);
 
	total_instructions_executed++;
};

//!Instruction slti behavior method.
void ac_behavior( slti )
{
	dbg_printf("slti r%d, r%d, %d\n", rt, rs, imm & 0xFFFF);
	// Set the RD if RS< IMM
	if( (ac_Sword) RB[rs] < (ac_Sword) imm )
		RB[rt] = 1;
	// Else reset RD
	else
		RB[rt] = 0;
	dbg_printf("Result = %#x\n", RB[rt]);

	// update pipeline 5
	update_pipeline((char*)"slti", -1, rt, rs, false);
 
	total_instructions_executed++;
};

//!Instruction sltiu behavior method.
void ac_behavior( sltiu )
{
	dbg_printf("sltiu r%d, r%d, %d\n", rt, rs, imm & 0xFFFF);
	// Set the RD if RS< IMM
	if( (ac_Uword) RB[rs] < (ac_Uword) imm )
		RB[rt] = 1;
	// Else reset RD
	else
		RB[rt] = 0;
	dbg_printf("Result = %#x\n", RB[rt]);

	// update pipeline 5
	update_pipeline((char*)"sltiu", -1, rt, rs, false);
 
	total_instructions_executed++;
};

//!Instruction andi behavior method.
void ac_behavior( andi )
{   
	dbg_printf("andi r%d, r%d, %d\n", rt, rs, imm & 0xFFFF);
	RB[rt] = RB[rs] & (imm & 0xFFFF) ;
	dbg_printf("Result = %#x\n", RB[rt]);

	// update pipeline 5
	update_pipeline((char*)"andi", -1, rt, rs, false);
 
	total_instructions_executed++;
};

//!Instruction ori behavior method.
void ac_behavior( ori )
{   
	dbg_printf("ori r%d, r%d, %d\n", rt, rs, imm & 0xFFFF);
	RB[rt] = RB[rs] | (imm & 0xFFFF) ;
	dbg_printf("Result = %#x\n", RB[rt]);

	// update pipeline 5
	update_pipeline((char*)"ori", -1, rt, rs, false);
 
	total_instructions_executed++;
};

//!Instruction xori behavior method.
void ac_behavior( xori )
{   
	dbg_printf("xori r%d, r%d, %d\n", rt, rs, imm & 0xFFFF);
	RB[rt] = RB[rs] ^ (imm & 0xFFFF) ;
	dbg_printf("Result = %#x\n", RB[rt]);

	// update pipeline 5
	update_pipeline((char*)"xori", -1, rt, rs, false);
 
	total_instructions_executed++;
};

//!Instruction lui behavior method.
void ac_behavior( lui )
{   
	dbg_printf("lui r%d, r%d, %d\n", rt, rs, imm & 0xFFFF);
	// Load a constant in the upper 16 bits of a register
	// To achieve the desired behaviour, the constant was shifted 16 bits left
	// and moved to the target register ( rt )
	RB[rt] = imm << 16;
	dbg_printf("Result = %#x\n", RB[rt]);

	// update pipeline 5
	update_pipeline((char*)"lui", -1, rt, rs, false);
 
	total_instructions_executed++;
};

//!Instruction add behavior method.
void ac_behavior( add )
{
	dbg_printf("add r%d, r%d, r%d\n", rd, rs, rt);
	RB[rd] = RB[rs] + RB[rt];
	dbg_printf("Result = %#x\n", RB[rd]);
	//Test overflow
	if ( ((RB[rs] & 0x80000000) == (RB[rd] & 0x80000000)) &&
			 ((RB[rd] & 0x80000000) != (RB[rt] & 0x80000000)) ) {
		fprintf(stderr, "EXCEPTION(add): integer overflow.\n"); exit(EXIT_FAILURE);
	}


	// update pipeline 5
	update_pipeline((char*)"add", rd, rt, rs, false);
 
 
	total_instructions_executed++;
};

//!Instruction addu behavior method.
void ac_behavior( addu )
{
	dbg_printf("addu r%d, r%d, r%d\n", rd, rs, rt);
	RB[rd] = RB[rs] + RB[rt];
	//cout << "  RS: " << (unsigned int)RB[rs] << " RT: " << (unsigned int)RB[rt] << endl;
	//cout << "  Result =  " <<  (unsigned int)RB[rd] <<endl;
	dbg_printf("Result = %#x\n", RB[rd]);

	// update pipeline 5
	update_pipeline((char*)"addu", rd, rt, rs, false);
 
	total_instructions_executed++;
};

//!Instruction sub behavior method.
void ac_behavior( sub )
{
	dbg_printf("sub r%d, r%d, r%d\n", rd, rs, rt);
	RB[rd] = RB[rs] - RB[rt];
	dbg_printf("Result = %#x\n", RB[rd]);
	//TODO: test integer overflow exception for sub

	// update pipeline 5
	update_pipeline((char*)"sub", rd, rt, rs, false);
 
	total_instructions_executed++;
};

//!Instruction subu behavior method.
void ac_behavior( subu )
{
	dbg_printf("subu r%d, r%d, r%d\n", rd, rs, rt);
	RB[rd] = RB[rs] - RB[rt];
	dbg_printf("Result = %#x\n", RB[rd]);

	// update pipeline 5
	update_pipeline((char*)"subu", rd, rt, rs, false);
 
	total_instructions_executed++;
};

//!Instruction slt behavior method.
void ac_behavior( slt )
{   
	dbg_printf("slt r%d, r%d, r%d\n", rd, rs, rt);
	// Set the RD if RS< RT
	if( (ac_Sword) RB[rs] < (ac_Sword) RB[rt] )
		RB[rd] = 1;
	// Else reset RD
	else
		RB[rd] = 0;
	dbg_printf("Result = %#x\n", RB[rd]);

	// update pipeline 5
	update_pipeline((char*)"slt", rd, rt, rs, false);
 
	total_instructions_executed++;
};

//!Instruction sltu behavior method.
void ac_behavior( sltu )
{
	dbg_printf("sltu r%d, r%d, r%d\n", rd, rs, rt);
	// Set the RD if RS < RT
	if( RB[rs] < RB[rt] )
		RB[rd] = 1;
	// Else reset RD
	else
		RB[rd] = 0;
	dbg_printf("Result = %#x\n", RB[rd]);

	// update pipeline 5
	update_pipeline((char*)"sltu", rd, rt, rs, false);
 
	total_instructions_executed++;
};

//!Instruction instr_and behavior method.
void ac_behavior( instr_and )
{
	dbg_printf("instr_and r%d, r%d, r%d\n", rd, rs, rt);
	RB[rd] = RB[rs] & RB[rt];
	dbg_printf("Result = %#x\n", RB[rd]);


	// update pipeline 5
	update_pipeline((char*)"instr_and", rd, rt, rs, false);
 
	total_instructions_executed++;
};

//!Instruction instr_or behavior method.
void ac_behavior( instr_or )
{
	dbg_printf("instr_or r%d, r%d, r%d\n", rd, rs, rt);
	RB[rd] = RB[rs] | RB[rt];
	dbg_printf("Result = %#x\n", RB[rd]);

	// update pipeline 5
	update_pipeline((char*)"instr_or", rd, rt, rs, false);
 
	total_instructions_executed++;
};

//!Instruction instr_xor behavior method.
void ac_behavior( instr_xor )
{
	dbg_printf("instr_xor r%d, r%d, r%d\n", rd, rs, rt);
	RB[rd] = RB[rs] ^ RB[rt];
	dbg_printf("Result = %#x\n", RB[rd]);

	// update pipeline 5
	update_pipeline((char*)"instr_xor", rd, rt, rs, false);
 
	total_instructions_executed++;
};

//!Instruction instr_nor behavior method.
void ac_behavior( instr_nor )
{
	dbg_printf("nor r%d, r%d, r%d\n", rd, rs, rt);
	RB[rd] = ~(RB[rs] | RB[rt]);
	dbg_printf("Result = %#x\n", RB[rd]);

	// update pipeline 5
	update_pipeline((char*)"instr_nor", rd, rt, rs, false);
 
	total_instructions_executed++;
};

//!Instruction nop behavior method.
void ac_behavior( nop )
{  
	dbg_printf("nop\n");

	// update pipeline 5
	update_pipeline((char*)"nop", -1, -1, -1, false);
 
	total_instructions_executed++;
};

//!Instruction sll behavior method.
void ac_behavior( sll )
{  
	dbg_printf("sll r%d, r%d, %d\n", rd, rs, shamt);
	RB[rd] = RB[rt] << shamt;
	dbg_printf("Result = %#x\n", RB[rd]);

	// update pipeline 5
	update_pipeline((char*)"sll", rd, rt, rs, false);
 
	total_instructions_executed++;
};

//!Instruction srl behavior method.
void ac_behavior( srl )
{
	dbg_printf("srl r%d, r%d, %d\n", rd, rs, shamt);
	RB[rd] = RB[rt] >> shamt;
	dbg_printf("Result = %#x\n", RB[rd]);

	// update pipeline 5
	update_pipeline((char*)"srl", rd, rt, rs, false);
 
	total_instructions_executed++;
};

//!Instruction sra behavior method.
void ac_behavior( sra )
{
	dbg_printf("sra r%d, r%d, %d\n", rd, rs, shamt);
	RB[rd] = (ac_Sword) RB[rt] >> shamt;
	dbg_printf("Result = %#x\n", RB[rd]);

	// update pipeline 5
	update_pipeline((char*)"sra", rd, rt, rs, false);
 
	total_instructions_executed++;
};

//!Instruction sllv behavior method.
void ac_behavior( sllv )
{
	dbg_printf("sllv r%d, r%d, r%d\n", rd, rt, rs);
	RB[rd] = RB[rt] << (RB[rs] & 0x1F);
	dbg_printf("Result = %#x\n", RB[rd]);

	// update pipeline 5
	update_pipeline((char*)"sllv", rd, rt, rs, false);
 
	total_instructions_executed++;
};

//!Instruction srlv behavior method.
void ac_behavior( srlv )
{
	dbg_printf("srlv r%d, r%d, r%d\n", rd, rt, rs);
	RB[rd] = RB[rt] >> (RB[rs] & 0x1F);
	dbg_printf("Result = %#x\n", RB[rd]);

	// update pipeline 5
	update_pipeline((char*)"srlv", rd, rt, rs, false);
 
	total_instructions_executed++;
};

//!Instruction srav behavior method.
void ac_behavior( srav )
{
	dbg_printf("srav r%d, r%d, r%d\n", rd, rt, rs);
	RB[rd] = (ac_Sword) RB[rt] >> (RB[rs] & 0x1F);
	dbg_printf("Result = %#x\n", RB[rd]);

	// update pipeline 5
	update_pipeline((char*)"srav", rd, rt, rs, false);
 
	total_instructions_executed++;
};

//!Instruction mult behavior method.
void ac_behavior( mult )
{
	dbg_printf("mult r%d, r%d\n", rs, rt);

	long long result;
	int half_result;

	result = (ac_Sword) RB[rs];
	result *= (ac_Sword) RB[rt];

	half_result = (result & 0xFFFFFFFF);
	// Register LO receives 32 less significant bits
	lo = half_result;

	half_result = ((result >> 32) & 0xFFFFFFFF);
	// Register HI receives 32 most significant bits
	hi = half_result ;

	dbg_printf("Result = %#llx\n", result);

	// update pipeline 5
	update_pipeline((char*)"mult", -1, rt, rs, false);
 
	total_instructions_executed++;
};

//!Instruction multu behavior method.
void ac_behavior( multu )
{
	dbg_printf("multu r%d, r%d\n", rs, rt);

	unsigned long long result;
	unsigned int half_result;

	result  = RB[rs];
	result *= RB[rt];

	half_result = (result & 0xFFFFFFFF);
	// Register LO receives 32 less significant bits
	lo = half_result;

	half_result = ((result>>32) & 0xFFFFFFFF);
	// Register HI receives 32 most significant bits
	hi = half_result ;

	dbg_printf("Result = %#llx\n", result);

	// update pipeline 5
	update_pipeline((char*)"multu", -1, rt, rs, false);
 
	total_instructions_executed++;
};

//!Instruction div behavior method.
void ac_behavior( div )
{
	dbg_printf("div r%d, r%d\n", rs, rt);
	// Register LO receives quotient
	lo = (ac_Sword) RB[rs] / (ac_Sword) RB[rt];
	// Register HI receives remainder
	hi = (ac_Sword) RB[rs] % (ac_Sword) RB[rt];

	// update pipeline 5
	update_pipeline((char*)"div", -1, rt, rs, false);
 
	total_instructions_executed++;
};

//!Instruction divu behavior method.
void ac_behavior( divu )
{
	dbg_printf("divu r%d, r%d\n", rs, rt);
	// Register LO receives quotient
	lo = RB[rs] / RB[rt];
	// Register HI receives remainder
	hi = RB[rs] % RB[rt];

	// update pipeline 5
	update_pipeline((char*)"divu", -1, rt, rs, false);
 
	total_instructions_executed++;
};

//!Instruction mfhi behavior method.
void ac_behavior( mfhi )
{
	dbg_printf("mfhi r%d\n", rd);
	RB[rd] = hi;
	dbg_printf("Result = %#x\n", RB[rd]);

	// update pipeline 5
	update_pipeline((char*)"mfhi", rd, -1, -1, false);
 
	total_instructions_executed++;
};

//!Instruction mthi behavior method.
void ac_behavior( mthi )
{
	dbg_printf("mthi r%d\n", rs);
	hi = RB[rs];
	dbg_printf("Result = %#x\n", (unsigned int) hi);

	// update pipeline 5
	update_pipeline((char*)"mthi", -1, rs, -1, false);
 
	total_instructions_executed++;
};

//!Instruction mflo behavior method.
void ac_behavior( mflo )
{
	dbg_printf("mflo r%d\n", rd);
	RB[rd] = lo;
	dbg_printf("Result = %#x\n", RB[rd]);

	// update pipeline 5
	update_pipeline((char*)"mflo", rd, -1, -1, false);
 
	total_instructions_executed++;
};

//!Instruction mtlo behavior method.
void ac_behavior( mtlo )
{
	dbg_printf("mtlo r%d\n", rs);
	lo = RB[rs];
	dbg_printf("Result = %#x\n", (unsigned int) lo);

	// update pipeline 5
	update_pipeline((char*)"mtlo", -1, rs, -1, false);
 
	total_instructions_executed++;
};

//!Instruction j behavior method.
void ac_behavior( j )
{
	dbg_printf("j %d\n", addr);
	addr = addr << 2;
#ifndef NO_NEED_PC_UPDATE
	npc =  (ac_pc & 0xF0000000) | addr;
#endif 
	dbg_printf("Target = %#x\n", (ac_pc & 0xF0000000) | addr );

	// update pipeline 5
	update_pipeline((char*)"j", -1, -1, -1, false);
 
	total_instructions_executed++;
};

//!Instruction jal behavior method.
void ac_behavior( jal )
{
	dbg_printf("jal %d\n", addr);
	// Save the value of PC + 8 (return address) in $ra ($31) and
	// jump to the address given by PC(31...28)||(addr<<2)
	// It must also flush the instructions that were loaded into the pipeline
	RB[Ra] = ac_pc+4; //ac_pc is pc+4, we need pc+8
		
	addr = addr << 2;
#ifndef NO_NEED_PC_UPDATE
	npc = (ac_pc & 0xF0000000) | addr;
#endif 
		
	dbg_printf("Target = %#x\n", (ac_pc & 0xF0000000) | addr );
	dbg_printf("Return = %#x\n", ac_pc+4);

	// update pipeline 5
	update_pipeline((char*)"jal", -1, -1, -1, false);
 
	total_instructions_executed++;
};

//!Instruction jr behavior method.
void ac_behavior( jr )
{
	dbg_printf("jr r%d\n", rs);
	// Jump to the address stored on the register reg[RS]
	// It must also flush the instructions that were loaded into the pipeline
#ifndef NO_NEED_PC_UPDATE
	npc = RB[rs], 1;
#endif 
	dbg_printf("Target = %#x\n", RB[rs]);

	// update pipeline 5
	update_pipeline((char*)"jr", -1, rs, -1, false);
 
	total_instructions_executed++;
};

//!Instruction jalr behavior method.
void ac_behavior( jalr )
{
	dbg_printf("jalr r%d, r%d\n", rd, rs);
	// Save the value of PC + 8(return address) in rd and
	// jump to the address given by [rs]

#ifndef NO_NEED_PC_UPDATE
	npc = RB[rs], 1;
#endif 
	dbg_printf("Target = %#x\n", RB[rs]);

	if( rd == 0 )  //If rd is not defined use default
		rd = Ra;
	RB[rd] = ac_pc+4;
	dbg_printf("Return = %#x\n", ac_pc+4);

	// update pipeline 5
	update_pipeline((char*)"jalr", rd, rs, -1, false);
 
	total_instructions_executed++;
};

//!Instruction beq behavior method.
void ac_behavior( beq )
{
	dbg_printf("beq r%d, r%d, %d\n", rt, rs, imm & 0xFFFF);
	if( RB[rs] == RB[rt] ){
#ifndef NO_NEED_PC_UPDATE
		npc = ac_pc + (imm<<2);
#endif

		branch_taken(ac_pc);
		dbg_printf("Taken to %#x\n", ac_pc + (imm<<2));
	} else {
		branch_not_taken(ac_pc);
	}

	// update pipeline 5
	update_pipeline((char*)"beq", -1, rs, rt, false);
 
	total_instructions_executed++;
};

//!Instruction bne behavior method.
void ac_behavior( bne )
{   
	dbg_printf("bne r%d, r%d, %d\n", rt, rs, imm & 0xFFFF);
	if( RB[rs] != RB[rt] ){
#ifndef NO_NEED_PC_UPDATE
		npc = ac_pc + (imm<<2);
#endif
		branch_taken(ac_pc);
		dbg_printf("Taken to %#x\n", ac_pc + (imm<<2));
	} else {
		branch_not_taken(ac_pc);
	}

	// update pipeline 5
	update_pipeline((char*)"bne", -1, rs, rt, false);
 
	total_instructions_executed++;
};

//!Instruction blez behavior method.
void ac_behavior( blez )
{
	dbg_printf("blez r%d, %d\n", rs, imm & 0xFFFF);
	if( (RB[rs] == 0 ) || (RB[rs]&0x80000000 ) ){
#ifndef NO_NEED_PC_UPDATE
		npc = ac_pc + (imm<<2), 1;
#endif
		branch_taken(ac_pc);
		dbg_printf("Taken to %#x\n", ac_pc + (imm<<2));
	} else {
		branch_not_taken(ac_pc);
	}

	// update pipeline 5
	update_pipeline((char*)"blez", -1, rs, -1, false);
 
	total_instructions_executed++;
};

//!Instruction bgtz behavior method.
void ac_behavior( bgtz )
{
	dbg_printf("bgtz r%d, %d\n", rs, imm & 0xFFFF);
	if( !(RB[rs] & 0x80000000) && (RB[rs]!=0) ){
#ifndef NO_NEED_PC_UPDATE
		npc = ac_pc + (imm<<2);
#endif
		branch_taken(ac_pc);
		dbg_printf("Taken to %#x\n", ac_pc + (imm<<2));
	} else {
		branch_not_taken(ac_pc);
	}

	// update pipeline 5
	update_pipeline((char*)"bgtz", -1, rs, -1, false);
 
	total_instructions_executed++;
};

//!Instruction bltz behavior method.
void ac_behavior( bltz )
{
	dbg_printf("bltz r%d, %d\n", rs, imm & 0xFFFF);
	if( RB[rs] & 0x80000000 ){
#ifndef NO_NEED_PC_UPDATE
		npc = ac_pc + (imm<<2);
#endif
		branch_taken(ac_pc);
		dbg_printf("Taken to %#x\n", ac_pc + (imm<<2));
	} else {
		branch_not_taken(ac_pc);
	}
	// update pipeline 5
	update_pipeline((char*)"bltz", -1, rs, -1, false);
 
	total_instructions_executed++;
};

//!Instruction bgez behavior method.
void ac_behavior( bgez )
{
	dbg_printf("bgez r%d, %d\n", rs, imm & 0xFFFF);
	if( !(RB[rs] & 0x80000000) ){
#ifndef NO_NEED_PC_UPDATE
		npc = ac_pc + (imm<<2);
#endif
		branch_taken(ac_pc);
		dbg_printf("Taken to %#x\n", ac_pc + (imm<<2));
	} else {
		branch_not_taken(ac_pc);
	}

	// update pipeline 5
	update_pipeline((char*)"bgez", -1, rs, -1, false);
 
	total_instructions_executed++;
};

//!Instruction bltzal behavior method.
void ac_behavior( bltzal )
{
	dbg_printf("bltzal r%d, %d\n", rs, imm & 0xFFFF);
	RB[Ra] = ac_pc+4; //ac_pc is pc+4, we need pc+8
	if( RB[rs] & 0x80000000 ){
#ifndef NO_NEED_PC_UPDATE
		npc = ac_pc + (imm<<2);
#endif
		branch_taken(ac_pc);
		dbg_printf("Taken to %#x\n", ac_pc + (imm<<2));
	} else {
		branch_not_taken(ac_pc);
	}
	dbg_printf("Return = %#x\n", ac_pc+4);

	// update pipeline 5
	update_pipeline((char*)"bltzal", -1, rs, -1, false);
 
	total_instructions_executed++;
};

//!Instruction bgezal behavior method.
void ac_behavior( bgezal )
{
	dbg_printf("bgezal r%d, %d\n", rs, imm & 0xFFFF);
	RB[Ra] = ac_pc+4; //ac_pc is pc+4, we need pc+8
	if( !(RB[rs] & 0x80000000) ){
#ifndef NO_NEED_PC_UPDATE
		npc = ac_pc + (imm<<2);
#endif
		branch_taken(ac_pc);
		dbg_printf("Taken to %#x\n", ac_pc + (imm<<2));
	} else {
		branch_not_taken(ac_pc);
	}
	dbg_printf("Return = %#x\n", ac_pc+4);

	// update pipeline 5
	update_pipeline((char*)"bgezal", -1, rs, -1, false);
 
	total_instructions_executed++;
};

//!Instruction sys_call behavior method.
void ac_behavior( sys_call )
{
	dbg_printf("syscall\n");
	stop();
}

//!Instruction instr_break behavior method.
void ac_behavior( instr_break )
{
	fprintf(stderr, "instr_break behavior not implemented.\n"); 
	exit(EXIT_FAILURE);
}

// Branch predictor methods
void branch_taken(int ac){
	// printf("Branch taken\n");
	// Static predictor
	number_stalls_normal += 1;

	// O BRANCH PREDICTION EH FEITO NO ID

	// aqui tem tambem um hazard de controle, normal
	number_stalls_control_hazards_5++;
	number_stalls_control_hazards_7++;

	// esse stall é dado no ID, para jogar o if que veio fora
	stall(pipeline_5, 1);
	stall(pipeline_7, 1);

	// Dynamic predictor (prediction buffer)
	if(pb[ac] == 2 || pb[ac] == 3){
		pb[ac] -= 2;
		number_stalls_pb += 1;

		 // stall no pipeline - depende do pipeline que é

		// PELAS MINHAS CONTAS, O SIMULADOR JA TEM A INSTRUÇÃO CORRETA, ENTÃO O OUTRO STALL OBRIGATORIO SE ENCARREGA DISSO

		// pipeline_5 toma o branch em EX 
		// hazard de controle
		number_stalls_control_hazards_5 += 1;
		stall(pipeline_5, 2);

		// para os outros pipelines

		// para o de 7 estagios, temos 3 stalls, pois o branch é tomado no estágio EX2
		number_stalls_control_hazards_7 += 2;
		stall(pipeline_7, 3);
		stall(pipeline_7, 3);

	} else if (pb[ac] == 1){
		pb[ac] -= 1;
	}

	// TLAP
	int oc = tlap_oc[ac] & 3; // last two occurrences
	if(tlap_pb[ac][oc] == 2 || tlap_pb[ac][oc] == 3){
		tlap_pb[ac][oc]-= 2;
		number_stalls_tlap += 1;


		// stall no pipeline - depende do pipeline que é

		// PELAS MINHAS CONTAS, O SIMULADOR JA TEM A INSTRUÇÃO CORRETA, ENTÃO O OUTRO STALL OBRIGATORIO SE ENCARREGA DISSO

		// pipeline_5 toma o branch em EX 
		// hazard de controle
		number_stalls_control_hazards_5 += 1;
		stall(pipeline_5, 2);

		// para os outros pipelines

		// para o de 7 estagios, temos 3 stalls, pois o branch é tomado no estágio EX2
		number_stalls_control_hazards_7 += 2;
		stall(pipeline_7, 3);
		stall(pipeline_7, 3);


	} else if (tlap_pb[ac][oc] == 1){
		tlap_pb[ac][oc] -= 1;
	}
	oc = (oc << 1) + 1;
	tlap_oc[ac] = oc;
}

void branch_not_taken(int ac){
	// printf("Branch not taken\n");

	// Dynamic predictor (prediction buffer)
	if(pb[ac] == 0 || pb[ac] == 1){
		pb[ac] += 2;
		number_stalls_pb += 1;

		// stall no pipeline - depende do pipeline que é

		// pipeline_5 toma o branch em EX 
		// hazard de controle
		number_stalls_control_hazards_5 += 2;
		stall(pipeline_5, 2);
		stall(pipeline_5, 2);

		// para os outros pipelines

		// para o de 7 estagios, temos 3 stalls, pois o branch é tomado no estágio EX2
		number_stalls_control_hazards_7 += 3;
		stall(pipeline_7, 3);
		stall(pipeline_7, 3);
		stall(pipeline_7, 3);


	} else if (pb[ac] == 2){
		pb[ac] += 1;
	}

	// TLAP
	int oc = tlap_oc[ac] & 3; // last two occurrences
	if(tlap_pb[ac][oc] == 0 || tlap_pb[ac][oc] == 1){
		tlap_pb[ac][oc]+= 2;
		number_stalls_tlap += 1;

		// stall no pipeline - depende do pipeline que é

		// pipeline_5 toma o branch em EX 
		// hazard de controle
		number_stalls_control_hazards_5 += 2;
		stall(pipeline_5, 2);
		stall(pipeline_5, 2);

		// para os outros pipelines

		// para o de 7 estagios, temos 3 stalls, pois o branch é tomado no estágio EX2
		number_stalls_control_hazards_7 += 3;
		stall(pipeline_7, 3);
		stall(pipeline_7, 3);
		stall(pipeline_7, 3);

	} else if (tlap_pb[ac][oc] == 2){
		tlap_pb[ac][oc] += 1;
	}
	oc = (oc << 1);
	tlap_oc[ac] = oc;
}
