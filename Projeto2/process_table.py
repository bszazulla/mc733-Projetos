raw_data_dijkstra = {'Configuracao': ['1', '2', '3', '4', '5', '6', '7', '8', '9'],
        'Cache': [252818364, 252818364, 252873774, 252873774, 252798524, 252798524, 252798524, 252859664, 252859664],
        'Branch-Predictor': [3828804 * 3, 3828804 * 4, 187087 * 3, 187087 * 4, 187087 * 3, 107051 * 4, 107051 * 3, 107051 * 3, 107051 * 4],
        'Data-Hazard': [63736915, 87493569, 53435801, 69492588, 63736915, 87493569, 53435801, 53435801, 69492588]}
        
raw_data_qsort = {'Configuracao': ['1', '2', '3', '4', '5', '6', '7', '8', '9'],
        'Cache': [229185486, 229185486, 227889336, 227889336, 223733056, 223733056, 223733056, 224742246, 224742246],
        'Branch-Predictor': [2021382 * 3, 2021382 * 4, 538078 * 3, 538078 * 4, 538078 * 3, 538078 * 4, 255268 * 3, 255268 * 3, 255268 * 4],
        'Data-Hazard': [49510049, 69895880, 42665683, 59056726, 49510049, 69895880, 42665683, 42665683, 59056726]}

raw_data_sha = {'Configuracao': ['1', '2', '3', '4', '5', '6', '7', '8', '9'],
        'Cache': [71420992, 71420992, 70641207, 70641207, 69923892, 69923892, 69923892, 67958577, 67958577],
        'Branch-Predictor': [525544 * 3, 525544 * 4, 69190 * 3, 69190 * 4, 69190 * 3, 69190 * 4, 69295 * 3, 69295 * 3, 69295 * 4],
        'Data-Hazard': [17056591, 23659496, 13909019, 15809145, 17056591, 23659496, 13909019, 13909019, 15809145]}

for i in range(9):
	print("* Configuracao " +  str(i + 1) + ":\n")
	print("| Eventos | dijkstra | qsort | sha |")
	print("| --- | --- | --- | --- |")
	print("| ciclos devido aos Hazards de Dados | " + str(raw_data_dijkstra['Data-Hazard'][i]) + " | " + str(raw_data_qsort['Data-Hazard'][i]) + " | " +  str(raw_data_sha['Data-Hazard'][i]) + " | ")
	print("| ciclos devido aos Hazards de Controle | " + str(raw_data_dijkstra['Branch-Predictor'][i]) + " | " + str(raw_data_qsort['Branch-Predictor'][i]) + " | " +  str(raw_data_sha['Branch-Predictor'][i]) + " | ")
	print("| ciclos devido acesso a memoria| " + str(raw_data_dijkstra['Cache'][i]) + " | " + str(raw_data_qsort['Cache'][i]) + " | " +  str(raw_data_sha['Cache'][i]) + " | ")
	print("| Total | " + str(raw_data_dijkstra['Data-Hazard'][i] + raw_data_dijkstra['Branch-Predictor'][i] + raw_data_dijkstra['Cache'][i]) + " | " + str(raw_data_qsort['Data-Hazard'][i] + raw_data_qsort['Branch-Predictor'][i] + raw_data_qsort['Cache'][i]) + " | " + str(raw_data_sha['Data-Hazard'][i] + raw_data_sha['Branch-Predictor'][i] + raw_data_sha['Cache'][i]) + " | ")
	print("\n")
