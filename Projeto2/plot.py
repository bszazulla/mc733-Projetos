import pandas as pd
import matplotlib.pyplot as plt
import numpy as np

raw_data_dijkstra = {'Configuracao': ['1', '2', '3', '4', '5', '6', '7', '8', '9'],
        'Cache': [252818364, 252818364, 252873774, 252873774, 252798524, 252798524, 252798524, 252859664, 252859664],
        'Branch-Predictor': [3828804 * 3, 3828804 * 4, 187087 * 3, 187087 * 4, 187087 * 3, 107051 * 4, 107051 * 3, 107051 * 3, 107051 * 4],
        'Data-Hazard': [63736915, 87493569, 53435801, 69492588, 63736915, 87493569, 53435801, 53435801, 69492588]}
        
raw_data_qsort = {'Configuracao': ['1', '2', '3', '4', '5', '6', '7', '8', '9'],
        'Cache': [229185486, 229185486, 227889336, 227889336, 223733056, 223733056, 223733056, 224742246, 224742246],
        'Branch-Predictor': [2021382 * 3, 2021382 * 4, 538078 * 3, 538078 * 4, 538078 * 3, 538078 * 4, 255268 * 3, 255268 * 3, 255268 * 4],
        'Data-Hazard': [49510049, 69895880, 42665683, 59056726, 49510049, 69895880, 42665683, 42665683, 59056726]}

raw_data_sha = {'Configuracao': ['1', '2', '3', '4', '5', '6', '7', '8', '9'],
        'Cache': [71420992, 71420992, 70641207, 70641207, 69923892, 69923892, 69923892, 67958577, 67958577],
        'Branch-Predictor': [525544 * 3, 525544 * 4, 69190 * 3, 69190 * 4, 69190 * 3, 69190 * 4, 69295 * 3, 69295 * 3, 69295 * 4],
        'Data-Hazard': [17056591, 23659496, 13909019, 15809145, 17056591, 23659496, 13909019, 13909019, 15809145]}
        
df = pd.DataFrame(raw_data_sha, columns = ['Configuracao', 'Cache', 'Branch-Predictor', 'Data-Hazard'])

# Create the general blog and the "subplots" i.e. the bars
f, ax1 = plt.subplots(1, figsize=(10,5))

# Set the bar width
bar_width = 0.75

# positions of the left bar-boundaries
bar_l = [i+1 for i in range(len(df['Cache']))]

# positions of the x-axis ticks (center of the bars as bar labels)
tick_pos = [i+(bar_width/2) for i in bar_l]

# Create a bar plot, in position bar_1
ax1.bar(bar_l,
        # using the pre_score data
        df['Cache'],
        # set the width
        width=bar_width,
        # with the label pre score
        label='cycles due to memory access',
        # with alpha 0.5
        alpha=0.5,
        # with color
        color="#F4561D")

# Create a bar plot, in position bar_1
ax1.bar(bar_l,
        # using the mid_score data
        df['Branch-Predictor'],
        # set the width
        width=bar_width,
        # with pre_score on the bottom
        bottom=df['Cache'],
        # with the label mid score
        label='cycles due to wrong prediction',
        # with alpha 0.5
        alpha=0.5,
        # with color
        color="#F1911E")

# Create a bar plot, in position bar_1
ax1.bar(bar_l,
        # using the post_score data
        df['Data-Hazard'],
        # set the width
        width=bar_width,
        # with pre_score and mid_score on the bottom
        bottom=[i+j for i,j in zip(df['Cache'],df['Branch-Predictor'])],
        # with the label post score
        label='cycles due to data hazards',
        # with alpha 0.5
        alpha=0.5,
        # with color
        color="#F1BD1A")

# set the x ticks with names
plt.xticks(tick_pos, df['Configuracao'])

# Set the label and legends
ax1.set_ylabel("Cycles")
ax1.set_xlabel("Configuration")
plt.legend(loc='upper left')

# Set a buffer around the edge
plt.xlim([min(tick_pos)-bar_width, max(tick_pos)+bar_width])

plt.show()
