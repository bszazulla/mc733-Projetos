#include <iostream>
#include <stdio.h>
extern "C" {
	#include "../d4-7/d4.h"
}

void doread(void* addr, d4cache* Cache) {
	d4memref R;
	R.address = (d4addr)addr;
	R.size = sizeof(double);
	R.accesstype = D4XREAD;
	d4ref(Cache, R);
}
void dowrite(void* addr, d4cache* Cache) {
	d4memref R;
	R.address = (d4addr)addr;
	R.size = sizeof(double);
	R.accesstype = D4XWRITE;
	d4ref(Cache, R);
}

void vector_sum(int *A, int *B, int *C, int n, d4cache* Cache) {
	for (unsigned i = 0; i < n; i++) {
		doread(&A[i], Cache);
		doread(&B[i], Cache);
		dowrite(&C[i], Cache);
		
		C[i] = A[i] + B[i];
	}
}

int main(int argc, char** argv) {

	d4cache* Mem;
	d4cache* L1;
	Mem = d4new(0);
	L1 = d4new(Mem);
	L1->name = "L1";
	L1->lg2blocksize = 8;
	L1->lg2subblocksize = 6;
	L1->lg2size = 20;
	L1->assoc = 2;
	L1->replacementf = d4rep_lru;
	L1->prefetchf = d4prefetch_none;
	L1->wallocf = d4walloc_always;
	L1->wbackf = d4wback_always;
	L1->name_replacement = L1->name_prefetch = L1->name_walloc = L1->name_wback = "L1";
	
	int r;
	if (0 != (r = d4setup())) {
		std::cout << "Failed\n";
		return 0;
	}

	int A[] = {1, 2};
	int B[] = {3, 4};
	int C[2];
	
	vector_sum(A, B, C, 2, L1);

	std::cout << "C: ";
	for(int i = 0; i < 2; i++)
		std::cout << C[i] << " ";
	std::cout << "\n";
	
	std::cout << L1->miss[D4XREAD]
	+ L1->miss[D4XWRITE]
	+ L1->miss[D4XINSTRN]
	+ L1->miss[D4XMISC]
	+ L1->miss[D4XREAD+D4PREFETCH]
	+ L1->miss[D4XWRITE+D4PREFETCH]
	+ L1->miss[D4XINSTRN+D4PREFETCH]
	+ L1->miss[D4XMISC+D4PREFETCH]
	<< " of " << 
	L1->fetch[D4XREAD]
	+ L1->fetch[D4XWRITE]
	+ L1->fetch[D4XINSTRN]
	+ L1->fetch[D4XMISC]
	+ L1->fetch[D4XREAD+D4PREFETCH]
	+ L1->fetch[D4XWRITE+D4PREFETCH]
	+ L1->fetch[D4XINSTRN+D4PREFETCH]
	+ L1->fetch[D4XMISC+D4PREFETCH]
	<<"\n";

	return 0;
}

