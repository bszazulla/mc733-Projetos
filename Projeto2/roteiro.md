<!---
# Roteiro - Projeto 2

Beatriz Sechin Zazulla - 154779

Henrique Alves - 138507

Matheus Jun Ota - 138889

WORKFLOW:
-cole a pasta Projeto2 em um diretorio compartilhado com a vm ou, caso vc utilize o pc do IC,
só copie a pasta Projeto2 para lá.

-para compilar o simulador basta entrar em Projeto/mips-2.4.0 e rodar 'acsim mips.ac -abi'; no arquivo make, fazer as seguintes modificações:

	1- substituir a linha INC_DIR para:
	INC_DIR := -I. `pkg-config --cflags systemc` `pkg-config --cflags archc` -L ../d4-7

	2- substituir a linha LIBS para:
	LIBS := $(LIB_SYSTEMC) $(LIB_ARCHC) $(LIB_POWERSC) $(LIB_DWARF) -ld4

-para rodar um programa do benchmark você pode utilizar o script run.sh disponibilizado, mas antes sete a seguinte variavel de ambiente:
export SIMULATOR="/path_to/Projeto2/mips-2.4.0/mips.x --load="

Configurações:

1)
Cache: 1
Branch: static
Pipeline: 5
Forward: sem

2)
Cache: 2
Branch: static
Pipeline: 5
Forward: com

3)
Cache: 2
Branch: 1 level
Pipeline: 7
Forward: com

4)
Cache: 3
Branch: 1 level
Pipeline: 7
Forward: sem

5)
Cache: 4
Branch: 2 level
Pipeline: 13
Forward: com

6)
Cache: 3
Branch: 2 level
Pipeline: 5
Forward: com

7)
Cache: 1
Branch: 2 level
Pipeline: 7
Forward: com

8)
Cache: 4
Branch: 2 level
Pipeline: 5
Forward: com

Configuracao das Caches:
1)
L1 - Dados:
	Size = 64KB
	Block Size = 32B
	Associativity = 4

L1 - Instrucoes:
	Size = 64KB
	Block Size = 32B
	Associativity = 4

L2:
	Size = 128KB
	Block Size = 64B
	Associativity = 4

2)
L1 - Dados:
	Size = 64KB
	Block Size = 32B
	Associativity = 4

L1 - Instrucoes:
	Size = 64KB
	Block Size = 32B
	Associativity = 4

L2:
	Size = 128KB
	Block Size = 64B
	Associativity = 4

L3:
	Size = 8MB
	Block Size = 64B
	Associativity = 4
	
3)
L1 - Dados:
	Size = 32KB
	Block Size = 64B
	Associativity = 8

L1 - Instrucoes:
	Size = 32KB
	Block Size = 64B
	Associativity = 8

L2:
	Size = 256KB
	Block Size = 64B
	Associativity = 8

4)
L1 - Dados:
	Size = 32KB
	Block Size = 64B
	Associativity = 8

L1 - Instrucoes:
	Size = 32KB
	Block Size = 64B
	Associativity = 8

L2:
	Size = 256KB
	Block Size = 64B
	Associativity = 8

L3:
	Size = 8MB
	Block Size = 64B
	Associativity = 8

Programas:
dijkstra, sha e qsort
	
Resuminho de hazards - para lembrar de MC732/722

- **Objetivo**: Contornar possíveis Control Hazards (enquanto outras instruções estão na pipeline, ser necessário tomar uma decisão de Branch).
    
- **Porque?**: Quando a instrução de Branch é interpretada (no segundo estágio), é necessário criar uma bolha na pipeline para podermos calcular a Branch (saber se deve-se realizar o salto ou seguir incrementando PC) - a ideia de Branch Prediction é evitar essas bolhas 'assumindo' que o Branch vá ser pego ou não, e assim causando bolhas apenas em casos contrários ao que se foi assumido.
    
    - **Como?**: Uma das estratégias para 'assumir' o que uma instrução de Branch vai fazer é determina-la conforme *sua posição/função no programa*; por exemplo, instruções de Branches em loops apontam para linhas anteriores, portanto, é possível assumir que instruções Branches que apontam para linhas passadas provavelmente irão acontecer (as bolhas só acontecerão desse jeito caso dê ruim e na verdade era pra continuar o programa sem realizar a Branch). Outro approach mais popular é *analizar dinamicamente* cada Branch específica; armazena-se o 'histórico' de cada Branch e usa-se seu comportamento mais recente para assumir se ela irá acontecer ou não. Processadores MIPS utilizam uma técnica chamada *Delayed Branch* - o que se faz é 'adiantar' instruções imediatamente depois do Branch que são mais rápidas (operações simples como Add, Sub, etc), permitindo que o 'cálculo' do Branch seja realizado antes da instrução adiantada ser realizada [ na real eu ainda preciso entender essa parte; qqer coisa tá na pg. 284 do Computer Organization and Design, 5th edition].
    
    - **Problemas**: Quanto maior uma pipeline, maior o custo de se assumir uma Branch erroneamente.
--->

# Projeto 2 - MC723 Laboratório de Sistemas Computacionais

Beatriz Sechin Zazulla - 154779

Henrique Alves - 138507

Matheus Jun Ota - 138889
    
## 1) Introdução
Utilizando um simulador MIPS escrito em ArchC, objetivamos nesse projeto analisar as mudanças no desempenho causadas por mudanças na arquitetura do processador.
Para realizar tal feito, as seguintes características de um processador foram consideradas:

* Tamanho do pipeline (5 ou 7 estágios)
* Configuração das caches (1, 2, 3 ou 4)
* Configuração do branch predictor (static, OSCP, TLOP)
* Forward (com ou sem)

Para analisar as alterações causadas por cada uma dessas características, foram geradas 9 configurações distintas:

| Configuração | Cache | Branch Predictor | Pipeline | Forward |  
| --- | --- | --- | --- | --- |
| 1 | 1 | static | 5 estágios | sem |
| 2 | 1 | static | 7 estágios | sem |
| 3 | 2 | OSCP | 5 estágios | com |
| 4 | 2 | OSCP | 7 estágios | com |
| 5 | 3 | OSCP | 5 estágios | sem |
| 6 | 3 | OSCP | 7 estágios | sem |
| 7 | 3 | TLOP | 5 estágios | com |
| 8 | 4 | TLOP | 5 estágios | com |
| 9 | 4 | TLOP | 7 estágios | com |   

Alterando, principalmente, o arquivo **mips_isa.cpp**, visamos implementar uma contagem de ciclos que reflita a configuração desejada.

## 2) Pipeline

Foi implementado 2 Pipelines de 5 estágios e de 7 estágios. O pipeline de 5 estágios foi baseado nos slides da disciplina
[MC732](http://www.ic.unicamp.br/~rodolfo/Cursos/mc722/2s2016/Chapter_04.pdf). Por outro lado o pipeline de 7 estágios
foi baseado em um exemplo da faculdade de Illinois, mais o material que consta na bibliografia de A. Patterson e J. Hennessy.

A seguir estão os estágios do pipeline de 5 estágios:

* IF: lê o endereço da próxima instrução a ser realizada.
* ID: envia a instrução para uma unidade de controle e lê os registradores necessários para a execução daquela instrução.
* EX: utiliza da unidade lógica-aritmética (ALU) para executar a operação ou calcular um endereço.
* MEM: lê ou escreve na memória de dados.
* WB: escreve o resultado da operação de volta nos registradores do estágio ID.

E os estágios do pipeline de 7 estágios (baseado no seguinte [exercício](https://courses.engr.illinois.edu/cs232/fa2009/7_stage_pipeline.pdf)):

* IF: lê o endereço da próxima instrução a ser realizada.
* ID: envia a instrução para uma unidade de controle e lê os registradores necessários para a execução daquela instrução.
* EX1: realiza operações aritméticas
* EX2: realiza operações de memória
* MT: resolve endereço de memória
* MM: lê ou escreve dados na memória
* WB: escreve o resultado da operação de volta nos registradores do estágio ID.

### 2.1) Hazards
Sabe-se que há, essencialmente, 3 tipos de hazards:

* Hazards estruturais
* Hazards de Dados
* Hazards de Controle

Hazards estruturais ocorrem quando há um conflito no acesso a um recurso da memória. Como, na nossa implementação, 
a simulação cria caches de instruções e dados separadas, não há hazards estruturais em nossa implementação (mais sobre
isso será descrito na **seção 3**); restando portanto, os hazards de controle e de dados. Os hazards de controle serão 
tratados com maior detalhe na **seção 4**, que descreve o Branch-Predicion; os hazards de dados, por outro lado, 
estão muito ligados a implementação do pipeline, e serão portanto, descritos aqui.

#### Hazards de Dados em um Pipeline de 5 estágios (IF/ID/EX/MEM/WB):

Os hazards de dados em um pipeline de 5 estágios ocorrem pois uma instrução deseja utilizar um registrador que ainda não está "pronto",
isto é, com o valor atualizado. Podemos classificar essas situações em:

* 1 - Instrução no estágio de pipeline ID/EX deseja utilizar o valor de um registrador que ainda está em EX/MEM:

	* 1a EX/MEM.RegisterRd = ID/EX.RegisterRs
	* 1b EX/MEM.RegisterRd = ID/EX.RegisterRt

* 2 - Instrução no estágio de pipeline ID/EX deseja utilizar o valor de um registrador que ainda está em MEM/WB (ainda não atualizou os valores, consideramos que isso é feito na saída do WB):
	
	* 2a MEM/WB.RegisterRd = ID/EX.RegisterRs
	* 2b MEM/WB.RegisterRd = ID/EX.RegisterRt
	
* 3 - Instrução no estágio de pipeline ID/EX é um load em um registrador que é utilizado em uma instrução que já está em IF/ID

	* 3a ID/EX.RegisterRt = IF/ID.RegisterRt
	* 3b ID/EX.RegisterRt = IF/ID.RegisterRs

Existem duas possíveis soluções para esse problema:

* adicionar bolhas (nop), basicamente dar stall no processador: adicionamos operações que não fazem nada (nop), para que quando a instrução chegue em ID/EX o valor do registrador já esteja pronto. Note que essa opção aumenta o número de ciclos de execução do programa. São hazards de dados.

* forwarding: redirecionamos de EX/MEM e de MEM/WB diretamente para o registrador ID/EX, assim não é necessário que a execução da instrução
passe o estágio WB do pipeline para que o valor do registrador esteja pronto. O forwarding diminui o número de bolhas, mas elas ainda são necessárias
pois hazards de dados com instruções que lêem da memória e em seguida utilizam esse valor (load-use data hazards) necessitam de uma bolha, mesmo que o
pipeline utilize forwarding. O cofigo trata esses tipos de instruções fora da condicional de forwarding e testa a seguinte condição:

Uma observação importante relativa à implementação desse método de contagem de ciclos de stall é que, na ausência de forwarding, temos o acúmulo de bolhas enquanto a instrução percorre o pipeline antes de chegar ao estágio WB e concluir a atualização do banco de registradores. Por isso a implementação se vale de vários "if's", e não de "if's-else's" seguidos.

#### Hazards de Dados em um Pipeline de 7 estágios (IF/ID/EX1/EX2/MT/MM/WB):

Devido ao maior número de estágios, o pipeline de 7 estágios possui mais condições em que podem ocorrer um hazard de dados. Entretanto, a ideia geral se mantém:
o hazard ocorre quando um dado acessado no estágio ID/EX ainda não está pronto. Veremos em maiores detalhes cada uma das condições:

* 1 - Instrução no estágio de pipeline ID/EX1 deseja utilizar o valor de um registrador que ainda está em EX1/EX2:

	* 1a EX1/EX2.RegisterRd = ID/EX1.RegisterRs
	* 1b EX1/EX2.RegisterRd = ID/EX1.RegisterRt
	
A bolha produzida por essa situação é inevitável, uma vez que o dado não está pronto em EX1/EX2. Portanto, essa condição é tratada fora do bloco de possível forwarding do código e um stall é computado.
	
* 2 - Instrução no estágio de pipeline ID/EX1 deseja utilizar o valor de um registrador que ainda está em EX2/MT:

	* 2a EX2/MT.RegisterRd = ID/EX1.RegisterRs
	* 2b EX2/MT.RegisterRd = ID/EX1.RegisterRt
	
Aqui, é possível fazer o uso de forwarding e transferir o dado já pronto em EX2/MT para ID/EX1. Nos testes sem forwarding, há o cômputo de um stall por hazards de dados.

* 3 - Instrução no estágio de pipeline ID/EX1 deseja utilizar o valor de um registrador que ainda está em MT/MM:

	* 3a MT/MM.RegisterRd = ID/EX1.RegisterRs
	* 3b MT/MM.RegisterRd = ID/EX1.RegisterRt
	
Nesta condição, ocorre algo parecido com o bloco anterior. Com forwarding, não há cômputo de stall, porém sem forwarding, temos mais um ciclo de espera no pipeline. 

* 4 - Instrução no estágio de pipeline ID/EX1 deseja utilizar o valor de um registrador que ainda está em MM/WB:

	* 4a MM/WB.RegisterRd = ID/EX1.RegisterRs
	* 4b MM/WB.RegisterRd = ID/EX1.RegisterRt
	
Novamente, cômputo de mais um ciclo em caso da impossibilidade de aplicar forwarding no pipeline.

Há, também, a implementação de hazards de load-use, assim como no pipeline de 5 estágios. No entanto, para o pipeline de 7 estágios, temos 3 testes diferentes:

* 5 - Instrução no estágio de pipeline ID/EX1 é um load em um registrador que é utilizado em uma instrução que já está em IF/ID

	* 5a ID/EX1.RegisterRt = IF/ID.RegisterRt
	* 5b ID/EX1.RegisterRt = IF/ID.RegisterRs

* 6 - Instrução no estágio de pipeline EX1/EX2 é um load em um registrador que é utilizado em uma instrução que já está em IF/ID

	* 6a EX1/EX2.RegisterRt = IF/ID.RegisterRt
	* 6b EX1/EX2.RegisterRt = IF/ID.RegisterRs

* 7 - Instrução no estágio de pipeline EX2/MT é um load em um registrador que é utilizado em uma instrução que já está em IF/ID

	* 7a EX2/MT.RegisterRt = IF/ID.RegisterRt
	* 7b EX2/MT.RegisterRt = IF/ID.RegisterRs


Uma observação importante acerca desse método de contagem de ciclos de stall (bem como foi observado no pipeline de 5 estágios) é que, tanto na ausência de forwarding como nos hazards de load-use, temos o acúmulo de bolhas enquanto a instrução percorre o pipeline antes de chegar ao estágio WB e concluir a atualização do banco de registradores. Por isso a implementação se vale de vários "if's", e não de "if's-else's" seguidos.
	
## 3) Cache 
Para simular a execução de uma cache utilizamos a biblioteca do software DineroIV. Utilizando a função **d4ref(d4cache, d4memref)** pode-se 
simular operações na cache simulada. Assim, para emular o comportamento esperado, basta que se altere as seções do código em **mips_isa.cpp** 
que implementam instruções de acesso a memória, para que elas também simulem operações na cache criada pelo DineroIV.

Utilizando como referência o [manual do processador Intel IA-32 e 64](http://www.intel.com/content/dam/www/public/us/en/documents/manuals/64-ia-32-architectures-optimization-manual.pdf),
as seguintes configurações de cache foram utilizadas:

* Configuração de Cache 1:

| Tipo da Cache | Tamanho | Tamanho do Bloco | Associatividade |
| --- | --- | --- | --- |
| L1 - dados | 64KB | 32B | 4 |
| L1 - instruções | 64KB | 32B | 4 |
| L2 | 128KB | 64B | 4 |

* Configuração de Cache 2:

| Tipo da Cache | Tamanho | Tamanho do Bloco | Associatividade |
| --- | --- | --- | --- |
| L1 - dados | 64KB | 32B | 4 |
| L1 - instruções | 64KB | 32B | 4 |
| L2 | 128KB | 64B | 4 |
| L3 | 8MB | 64B | 4 |

* Configuração de Cache 3:

| Tipo da Cache | Tamanho | Tamanho do Bloco | Associatividade |
| --- | --- | --- | --- |
| L1 - dados | 32KB | 64B | 8 |
| L1 - instruções | 32KB | 64B | 8 |
| L2 | 265KB | 64B | 8 |

* Configuração de Cache 4:

| Tipo da Cache | Tamanho | Tamanho do Bloco | Associatividade |
| --- | --- | --- | --- |
| L1 - dados | 32KB | 64B | 8 |
| L1 - instruções | 32KB | 64B | 8 |
| L2 | 265KB | 64B | 8 |
| L3 | 8MB | 64B | 8 |

Para contagem de ciclos em cada acesso a memória utilizamos como base o processador Core i7 Xeon 5500 e instruções do tipo (d4cache) -> fetch[D4X*],
onde (d4cache) denota uma variável do tipo d4cache e D4X* pode ser D4XINSTRN, D4XREAD ou D4XWRITE.

| Tipo da Cache | Ciclos necessários para acessá-la |
| --- | --- |
| L1 dados | 4 |
| L1 instruções | 4 |
| L2 | 10 |
| L3 | 65 |
| RAM | 120 |

## 4) Branch Prediction:

Para melhor entender a variação de stalls com Branch Predictors diferentes, foram utilizadas três estratégias:

### 4.1) Static Predictor

Um Branch Predictor que 'assume' que nenhum pulo condicional será pego; assim, em casos aonde metade das branches seríam utilizadas no programa, 
o Branch Predictor estático teria 50% de eficiência. O Predictor estático serve como a base de números pela qual iremos comparar os Branches 
dinâmicos.

### 4.2) One-level Saturating Counter Predictor

One-level Saturating Counter Predictor (OSCP) funciona através de uma máquina de estados para cada instrução de Branch, correspondente as últimas
ações do próprio Branch; os estados possíveis são Fortemente Favorável (grande chance do Branch ser utilizado), Fracamente Favorável (pouca chance
do Branch ser utilizado), Fracamente Desfavorável (pouca chance do Branch não ser utilizado) e Fortemente Desfavorável (grande chance do Branch não
ser utilizado), correspondentes a um número inteiro de 0-3. Cada instrução de Branch no simulador irá assumir o estado de seu Predictor, e após ser
calculado será atualizado de forma correspondente (podendo ser 'beneficiado' em um por ter acertado ou 'penalizado' em dois por ter errado a prediction).

### 4.3) Two-level Adaptive Predictor

Two-level Adaptive Predictor (TLOP) implementa em cima do OSCP, com a diferença de que cada Branch irá guardar o 'histórico' de seus dois últimos
resultados em um vetor de dois bits, e cada combinação possível de bits no seu histórico sendo um TLOP. Dessa maneira, é possível se aproximar de 
predições em Branches que possuem comportamento periódico, ao custo de aproximadamente 4x mais memória para cada instrução de Branch.

### 4.4) Implementação dos Predictors

Em cada instrução de Branch do ArchC, foi feito uma chamada para os métodos correspondentes às duas possibilidades de retorno: *branch_taken* e 
*branch_not_taken*; em ambos os métodos, as variáveis globais dos Predictors são atualizadas de forma como deveríam se comportar, e em caso de uma 
predição errada, o contador correspondente ao Predictor é atualizado. No caso dos Predictors dinâmicos, foi necessário o uso de um dicionário em 
que as chaves são o PC de cada Branch encontrado; o valor no OSCP corresponde à máquina de estados, e o valor no TLOP corresponde à um vetor de 4 
inteiros, cada um com uma máquina de estados (o histórico de cada branch é guardado em um segundo dicionário).

### 4.5) Atualização de stalls no pipeline

São atualizados os stalls em ambos os pipelines quando as previsões de branches erram. Consideramos que os branches são tomados em EX (para o pipeline de 5 estágios) e em EX2 (para o pipeline de 7 estágios).

## 5) Resultados e Análise:

Para realizar uma análise dos resultados executamos os programas **dijkstra**, **sha** e **qsort** no simulador do MIPS.
Contamos então os ciclos devido aos hazards de dados (que está associado com o modelo de forwarding e pipeline utilizado na configuração), os ciclos devido
aos hazards de controle (associado com o modelo do branch-predictor) e os ciclos necessários para acesso a memória (associado ao modelo de cache utilizado).

### 5.1) Tabelas
A seguir seguem as tabelas com o desempenho de cada configuração:

* Configuracao 1:

| Eventos | dijkstra | qsort | sha |
| --- | --- | --- | --- |
| ciclos devido aos Hazards de Dados | 63736915 | 49510049 | 17056591 | 
| ciclos devido aos Hazards de Controle | 11486412 | 6064146 | 1576632 | 
| ciclos devido acesso a memoria| 252818364 | 229185486 | 71420992 | 
| Total | 328041691 | 284759681 | 90054215 | 


* Configuracao 2:

| Eventos | dijkstra | qsort | sha |
| --- | --- | --- | --- |
| ciclos devido aos Hazards de Dados | 87493569 | 69895880 | 23659496 | 
| ciclos devido aos Hazards de Controle | 15315216 | 8085528 | 2102176 | 
| ciclos devido acesso a memoria| 252818364 | 229185486 | 71420992 | 
| Total | 355627149 | 307166894 | 97182664 | 


* Configuracao 3:

| Eventos | dijkstra | qsort | sha |
| --- | --- | --- | --- |
| ciclos devido aos Hazards de Dados | 53435801 | 42665683 | 13909019 | 
| ciclos devido aos Hazards de Controle | 561261 | 1614234 | 207570 | 
| ciclos devido acesso a memoria| 252873774 | 227889336 | 70641207 | 
| Total | 306870836 | 272169253 | 84757796 | 


* Configuracao 4:

| Eventos | dijkstra | qsort | sha |
| --- | --- | --- | --- |
| ciclos devido aos Hazards de Dados | 69492588 | 59056726 | 15809145 | 
| ciclos devido aos Hazards de Controle | 748348 | 2152312 | 276760 | 
| ciclos devido acesso a memoria| 252873774 | 227889336 | 70641207 | 
| Total | 323114710 | 289098374 | 86727112 | 


* Configuracao 5:

| Eventos | dijkstra | qsort | sha |
| --- | --- | --- | --- |
| ciclos devido aos Hazards de Dados | 63736915 | 49510049 | 17056591 | 
| ciclos devido aos Hazards de Controle | 561261 | 1614234 | 207570 | 
| ciclos devido acesso a memoria| 252798524 | 223733056 | 69923892 | 
| Total | 317096700 | 274857339 | 87188053 | 


* Configuracao 6:

| Eventos | dijkstra | qsort | sha |
| --- | --- | --- | --- |
| ciclos devido aos Hazards de Dados | 87493569 | 69895880 | 23659496 | 
| ciclos devido aos Hazards de Controle | 428204 | 2152312 | 276760 | 
| ciclos devido acesso a memoria| 252798524 | 223733056 | 69923892 | 
| Total | 340720297 | 295781248 | 93860148 | 


* Configuracao 7:

| Eventos | dijkstra | qsort | sha |
| --- | --- | --- | --- |
| ciclos devido aos Hazards de Dados | 53435801 | 42665683 | 13909019 | 
| ciclos devido aos Hazards de Controle | 321153 | 765804 | 207885 | 
| ciclos devido acesso a memoria| 252798524 | 223733056 | 69923892 | 
| Total | 306555478 | 267164543 | 84040796 | 


* Configuracao 8:

| Eventos | dijkstra | qsort | sha |
| --- | --- | --- | --- |
| ciclos devido aos Hazards de Dados | 53435801 | 42665683 | 13909019 | 
| ciclos devido aos Hazards de Controle | 321153 | 765804 | 207885 | 
| ciclos devido acesso a memoria| 252859664 | 224742246 | 67958577 | 
| Total | 306616618 | 268173733 | 82075481 | 


* Configuracao 9:

| Eventos | dijkstra | qsort | sha |
| --- | --- | --- | --- |
| ciclos devido aos Hazards de Dados | 69492588 | 59056726 | 15809145 | 
| ciclos devido aos Hazards de Controle | 428204 | 1021072 | 277180 | 
| ciclos devido acesso a memoria| 252859664 | 224742246 | 67958577 | 
| Total | 322780456 | 284820044 | 84044902 |

### 5.2) Gráficos

Também foram criados gráficos, para facilitar a comparação entre as configurações de processadores:

* dijkstra

![graf1](dijkstra.png)
[link](dijkstra.png)

* qsort

![graf1](qsort.png)
[link](qsort.png)

* sha  

![graf1](sha.png)
[link](sha.png)

### 5.3) Análise

Observando o gráfico nota-se que apesar do acesso a memória ser o fator que mais contribuiu para a contagem de ciclos, 
alterar a arquitetura das caches não trouxe grandes mudanças. Ainda assim, observando o gráfico para o programa **sha**
notamos que a configuração de cache 4 possui o melhor desempenho, o que condiz com o esperado, pois essa é a cache que
segue mais fielmente o modelo de caches utilizados pelos processadores Intel IA-32 e 64.

Por outro lado, é claro pelos gráficos que o branch-predictor do tipo static é o pior dos três. Somente nas configurações 1 e 2
a contagem de ciclos devidos aos erros do branch-predictor foram relevantes frente ao total de ciclos consumido pelo programa.
Entre o OSCP e o TLOP, entretanto, não houve uma diferença de desempenho significativa se comparados à magnitude do predictor estático;
porém, TLOP possui uma performance geral melhor que a de OSCP. Vale a pena lembrar que ambos os branch-predictors dinâmicos usam memória local
de cada instrução de Branch para agir de acordo - portanto, a simulação feita considera o caso ideal de memória para todas as instruções
de Branches. Como o TLOP testado usa uma máquina de estados OSCP para cada combinação possível de bits no histórico de cada Branch (2 bits),
ela gasta aproximadamente 4x mais memória que a OSCP - numa implementação em Hardware, o custo-benefício deveria ser levado em consideração.

Outro recurso que se demonstrou eficiente foi o uso do forwarding. Por exemplo, as configurações 3 e 5 possuem a mesma configuração com
exceção de que esta utiliza forwarding e aquela não. Observando os gráficos, em todos os programas, notamos que a configuração 5
obteve um desempenho inferior a configuração 3 por um número relevante de ciclos. Entretanto, observando a configuração 6 notamos
que o pipeline criado pelo grupo com 7 estágios não obteve melhorias no desempenho, na verdade, ele foi significantemente pior que
o pipeline de 5 estágios. 
  
## 6) Referências
[1] Patterson, David A., and John L. Hennessy. "Computer organization and design." Morgan Kaufmann (2007): 474-476.

[2] Slides do Professor Rodolfo Azevedo da Disciplina MC723

[3] http://www.ic.unicamp.br/~rodolfo/mo801/04-dinero.pdf

[4] http://www.cs.utexas.edu/~pingali/CS378/2012fa/2012fa-assignments/assignment2.html
